#//**********************************************************************************************************
#// Evaluation Module
#// apply (Eq _ _) performs way too many evaluations, mode.force could be applied on the base terms,
#// then stepping apply on each new term created by And will do, just like for Reduction and Weakening.
#//**********************************************************************************************************
\q S -> {

	#//**********************************************************************
	#// helpers *************************************************************
	#//**********************************************************************
	_ @ unifyDone	= curry q.ok
	_ @ shift	= \f j -> \sig eqs -> f \x -> j x sig eqs
	_ @ try		= \f j -> \sig eqs -> f unifyDone sig eqs . q.either (cst q.zer) $ uncurry j
	_ @ strictAlg	= {eval: S.strict, force: cst (@), lazy: cst (@), strict: S.strict}
	_ @ lazyAlg	= {eval: cst (@), force: S.lazy, lazy: cst (@), strict: S.lazy}

	#//**********************************************************************
	#// evals ***************************************************************
	#//**********************************************************************
	done @ _	= \expr sig eqs		-> q.one {expr, {:sig, :eqs}}
	eval @ _	= \rho expr j		-> S.eval rho expr j
	lazy @ _	= \rho expr j		-> functor lazyAlg rho expr \expr -> S.apply rho expr j
	strict @ _	= \rho expr j		-> functor strictAlg rho expr \expr -> S.apply rho expr j

	#//**********************************************************************
	#// functor *************************************************************
	#//**********************************************************************
	functor		= \mode @ S, rho expr j	-> expr @ \:
		expr				-> GExprF.genk (S.eval rho) expr j :
		Var v / A.in v rho		-> S.force rho rho[v] $ j . Ide
		Suc a				-> GExprF.genk (S.strict rho) expr j
		Pre a				-> GExprF.genk (S.strict rho) expr j
		Pil a				-> GExprF.genk (S.strict rho) expr j
		Pir a				-> GExprF.genk (S.strict rho) expr j
		Equ l r				-> GExprF.genk (S.strict rho) expr j
		Amb a				-> GExprF.genk (S.strict rho) expr j
		Alt l r				-> GExprF.genk (S.lazy rho) expr j
		Let v b				-> GExprF.genk (S.lazy rho) expr j
		Lam v b				-> GExprF.genk (S.lazy rho) expr j
		App f a				-> S.strict rho f \f -> S.eval rho a \a -> j $ App f a
		Wkn v e b			-> S.strict rho e \e -> S.lazy rho b \b -> j $ Wkn v e b
		Ife c l r			-> S.strict rho c \c -> S.lazy rho l \l -> S.lazy rho r \r -> j $ Ife c l r
		Ifn c l r			-> S.strict rho c \c -> S.lazy rho l \l -> S.lazy rho r \r -> j $ Ifn c l r
		Uni l r a			-> S.strict rho l \l -> S.strict rho r \r -> S.lazy rho a \a -> j $ Uni l r a

	#//**********************************************************************
	#// apply ***************************************************************
	#//**********************************************************************
	apply @ _	= \rho expr j		-> expr @ \:
		#//**************************************************************
		#// Default *****************************************************
		#//**************************************************************
		expr				-> j expr :
		#//**************************************************************
		#// Non Determinism / Logic Programming *************************
		#//**************************************************************
		Ide e				-> j e
		Wkn v e b			-> shift (S.replace [(v): e] b) \b -> S.eval rho b j
		Amb Nil				-> \sig eqs -> q.zer
		Amb (Cns a as)			-> \sig eqs -> j a sig eqs `q.plus` S.apply rho (Amb as) j sig eqs
		Alt l r				-> \sig eqs -> S.eval rho l j sig eqs `q.plus` S.eval rho r j sig eqs
		Let v b				-> shift (S.mkVar v) \w -> shift (S.replace [(v): w] b) \b -> S.eval rho b j
		Uni l r e			-> try (S.unify l r) $ try S.verify \sig0 eqs0 -> S.replace sig0 e \e -> S.eval rho e j sig0 eqs0
		#//**************************************************************
		#// Application *************************************************
		#//**************************************************************
		App (Lam v b) a			-> shift (S.replace [(v): a] b) \b -> S.eval rho b j
		App (Typ n as) a		-> j $ Typ n $ L.concat as (a,)
		App (Dat n as) a		-> j $ Dat n $ L.concat as (a,)
		#//**************************************************************
		#// Reflexivity *************************************************
		#//**************************************************************
		Equ (Boo b) (Boo c)		-> j $ Boo $ b == c ? 1 : 0
		Equ (Int n) (Int m)		-> j $ Boo $ n == m ? 1 : 0
		Equ (Cpl al bl) (Cpl ar br)	-> S.eval rho (And (Equ al ar) $ Equ bl br) j
		Equ Nil Nil			-> j $ Boo 1
		Equ (Cns _ _) Nil		-> j $ Boo 0
		Equ Nil (Cns _ _)		-> j $ Boo 0
		Equ (Cns a as) (Cns b bs)	-> S.eval rho (And (Equ a b) (Equ as bs)) j
		Equ (Dat n _) (Dat m _) /m != n	-> j $ Boo 0
		Equ (Typ n _) (Typ m _) /m != n	-> j $ Boo 0
		Equ (Dat n as) (Dat m bs)	-> S.eval rho (L.foldr And (Boo 1) $ L.fzip Equ as bs) j
		Equ (Typ n as) (Typ m bs)	-> S.eval rho (L.foldr And (Boo 1) $ L.fzip Equ as bs) j
		#//**************************************************************
		#// List / Pair / Int / Boo *************************************
		#//**************************************************************
		Pil (Cpl l _)			-> j l
		Pir (Cpl _ r)			-> j r
		Suc (Int n)			-> j $ Int (n+1)
		Pre (Int n)			-> j $ Int (n-1)
		Ife (Boo 1) tru fal		-> S.eval rho tru j
		Ife (Boo 0) tru fal		-> S.eval rho fal j
		Ifn Nil nil cns			-> S.eval rho nil j
		Ifn (Cns a as) nil cns		-> S.eval rho (App (App cns a) as) j
		Ifn x l r			-> die {x, l, r}
		#//**************************************************************

}
