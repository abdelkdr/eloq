// START MATCH <?php
(function ($EXPR_TO_MATCH) {
	{L.implode "\n\t" $ L.mapf fbs \b -> "if ($RESULT_MATCH = {indents 1 $ (#tpl('Match.Branch.php'))}($EXPR_TO_MATCH)) return $RESULT_MATCH[0];"}
	throw new \Exception('No pattern in context \"{data.ctx}\" : ' . $this::toString($EXPR_TO_MATCH));
})({e})
// END MATCH
