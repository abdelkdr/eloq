// \header body -> <?php
return function ($_scope = []) {

	return (new class($_scope) {

		private function lambda($_scope) {
			$self = $this;
			{indents 2 header}
			return {indents 3 body};
		}

		static private $singletons = [];
		static $structure, $nil;
		public $nspace = [];

		function __construct($_scope) {
			self::$structure = self::getStructure('INSTANCE', 0, 'INSTANCE', []);
			self::$nil = self::getStructure('Data', 0, '()', []);
			$this->nspace = $this->lambda($this->scope($_scope));
			set_error_handler(function ( $errno, $errstr, $errfile, $errline ) {
				throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
			});
		}

		static function funConv($argc, $fun, $args = []) {
			if (!$argc)
				return $fun(...$args);
			return function ($x) use($argc, $args, $fun) {
				return self::funConv($argc - 1, $fun, array_merge($args, [$x]));
			};
		}

		function scope ($scope = []) {
			return $scope = [
				'reimp' => function ($f) {return function (...$args) use($f) { return array_reduce($args, function($f, $a) {return $f($a);}, $f); };},
				'implementation' => self::funConv(2, [$this, 'funConv']),
				'toString' => self::funConv(1, [$this, 'toString']),
				'include' => function ($e) { return include($e); },
				'eval' => function ($code) { return eval($code); },
				'die' => function ($e) { die(substr(is_string($e) ? $e : $this->toString($e), 0, 10000000)); },
				'echo' => function ($e) { echo substr(is_string($e) ? $e : $this->toString($e), 0, 10000000), "\n"; ob_flush(); return true; },
				'preg_match' => self::funConv(2, function($patt, $str) {preg_match($patt, $str, $m); return $m;}),
				'throw' => function($e) {throw new Exception($e);},
			];
		}

		static function just($a) {
			return self::getStructure('Data', 0, ':>', [$a, self::$nil]);
		}

		static function cons($a, $b) {
			return self::getStructure('Data', 0, ':>', [$a, $b]);
		}

		static function getStructure($kind, $arity, $name, $args) {

			if ( !$args ) {
				$k = "$arity:$kind:$name";
				if (!empty(self::$singletons[$k]))
					return self::$singletons[$k];
			}

			$a = (new class ($kind, $arity, $name, $args) {

				public $kind = "", $arity, $name, $args;

				public function __construct ($kind, $arity, $name, $args = []) {
					$this->kind = $kind;
					$this->arity = $arity;
					$this->name = $name;
					$this->args = $args;
				}

				function __invoke ( $arg ) {
					if ( $this->arity )
						return new static($this->kind, $this->arity -1, $this->name, array_merge($this->args, [$arg]));
					throw new \Exception("Can't apply {$this->kind} constructor \"{$this->name}\" anymore, arity exceeded !");
				}

			});

			if ( !$args )
				self::$singletons[$k] = $a;
			return $a;

		}

		function toStringList ( $data, $ind, $visited ) {
			if ( array_reduce($visited, function($a, $v) use($data){return $a || $v ===$data;}, false) )
				return ["<RECURSION>"];
			$visited[] = $data;

			switch(true) {
				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
					return array_merge([$this->toString($data->args[0], $ind, $visited)], $this->toStringList($data->args[1], $ind, $visited));
				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
					return [];
			}
		}

		function toString ( $data, $ind = "", $visited = [] ) {

			if ( array_reduce($visited, function($a, $v) use($data){return $a || $v ===$data;}, false) )
				return "<RECURSION>";
			$visited[] = $data;

			switch ( true ) {

				case is_null($data):
					return "null";

				case is_bool($data):
					return $data ? "true" : "false";

				case is_numeric($data):
					return "$data";

				case is_string($data):
					return '"' . str_replace(['\\', '"'], ['\\\\', '\\"'], $data) . '"';

				case is_array($data) && !$data:
					return "[]";

				case is_array($data) && is_string(array_keys($data)[0]):
					return "{" . implode("", array_map(
						function ( $k, $v ) use ($ind, $visited, $data) { return "\n\t$ind$k: " . $this->toString($v, "$ind\t", $visited) . ","; },
						array_keys($data),
						$data
					)) . "\n$ind}";

				case is_array($data):
					return "{" . implode("", array_map(
						function ($v) use ($ind, $visited, $data) {
							return "\n\t$ind" . $this->toString($v, "$ind\t", $visited) . ",";
						},
						$data
					)) . "\n$ind}";

				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
					array_pop($visited);
					$rrr = $this->toStringList($data, "$ind\t", $visited);
					$rrt = implode(", ",  $rrr);
					if ( strlen($rrt) > 110 )
						return "(\n$ind\t" . implode(",\n$ind\t", $rrr) . "\n$ind)";
					return "(" . $rrt . ")";

				case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
					return '()';

				case $data instanceof self::$structure && $data->kind === 'Data':
					if ( !$data->args )
						return "{$data->name}";
					return "({$data->name} " . implode(" ",
						array_map(function ($e) use ($ind, $visited, $data) { return $this->toString($e, "$ind", $visited); },
						$data->args
					)) . ")";

				case $data instanceof self::$structure && $data->kind === 'Type':
					return "(#{$data->name}" . (!$data->args ? '' : ' ' . implode(' ', array_map(
						function ($e) use($ind, $visited, $data) { return $this->toString($e, $ind, $visited); },
						$data->args
					))) . ')';

				 case $data instanceof \Closure:
					$clo = new ReflectionFunction($data);
					$params = implode(", ", array_map(function($a) {return $a->name;}, $clo->getParameters()));
					$closeds = implode(', ', array_keys($clo->getStaticVariables()));
					return "(# \\$params -> <CLOSURE>, [$closeds])";

				default: return sprintf('#%1$s%2$s', get_class($data), self::toString((array) $data, "$ind", $visited));

			}
		}

	});

};
