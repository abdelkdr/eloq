// START BRANCH
(function ($VALUE_TO_MATCH) {
	//BRANCH PATTERN
	if ( !{indents 1 $ compilePattern b.patt "$VALUE_TO_MATCH"} ) return;
	//BRANCH GUARD
	{M.from b.gard "" \h -> "if ( !({h.code}) ) return;"}
	// BRANCH EXP
	return [{indents 1 b.expr.code}];
})
// END BRANCH
