// \header body -> <?php
(function($self, $_scope){try {
	{header}
	return {indents 1 body};
} catch (\Throwable $e) {
	throw new \Exception("Error in embedded expression on line {data.nfo.line}:{data.nfo.col} in file '{data.nfo.file}'", 0, $e);
}})