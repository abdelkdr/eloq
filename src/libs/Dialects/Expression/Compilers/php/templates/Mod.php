// \header body -> <?php

if ( !function_exists("array_replace_rec") ) {
	function array_replace_rec($arr, $keys, $val) {

		if ( !$keys )
			return $val;

		if ( array_key_exists($keys[0], $arr) )
			$sub = $arr[$keys[0]];

		elseif ( count($keys) > 1 )
			$sub = [];

		else $sub = $val;

		return array_replace($arr, [$keys[0] => array_replace_rec($sub, array_slice($keys, 1), $val)]);

	}
}

if ( !function_exists("setContext") ) {
	class LinkedList {
		public $a, $b;
		function __construct($a, $b) {
			$this->a = $a;
			$this->b = $b;
		}
	}

	global $XprCtx;
	$XprCtx = [];
	function setContext($ctx) {
		global $XprCtx;
		$XprCtx = new LinkedList($ctx, $XprCtx);
		return function($e) {
			global $XprCtx;
			$XprCtx = $XprCtx->b;
			return $e;
		};
	}
}

if ( !function_exists("myuasort") ) {
	function myuasort($f, $arr) {
		uasort($arr, function($a, $b) use($f) { return $f($a)($b); });
		return $arr;
	}
}

if ( !class_exists("DispatchException") ) {
	class DispatchException extends Exception {}
}

return function ($_scope = [], $parent = null) use ($eloq_version) {

	return (new class($_scope, $parent, $eloq_version) {

		private function lambda($_scope) {
			$self = $this;
			{indents 2 header}
			return {indents 3 body};
		}

		static private $singletons = [];
		static $structure, $nil;
		public $nspace = [];
		public static $parent = null;
		public static $struc = null;
		public static $eloq_version;
		public static $XprCtx;

		public static function setContext($ctx) {
			global $XprCtx;
			$tmp = $XprCtx;
			$XprCtx = $ctx;
			return function($e) use($tmp) {
				global $XprCtx;
				$XprCtx = $tmp;
				return $e;
			};
		}

		function __construct($_scope, $parent, $eloq_version) {
			self::$eloq_version = $eloq_version;
			self::$parent = $parent;
			self::$struc = $parent ? $parent::$structure : null;
			self::$structure = $parent ? $parent::$structure : self::getStructure('INSTANCE', 0, 'INSTANCE', []);
			self::$nil = $parent ? $parent::$nil : self::getStructure('Data', 0, '()', []);
			$this->nspace = $this->lambda($this->scope($_scope));
			set_error_handler(function ( $errno, $errstr, $errfile, $errline ) {
				throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
			});
		}

		static function funConv($argc, $fun, $args = []) {
			if (!$argc)
				return $fun(...$args);
			return function ($x) use($argc, $args, $fun) {
				return self::funConv($argc - 1, $fun, array_merge($args, [$x]));
			};
		}

		function scope ($scope = []) {
			return $scope = [
				'__version__' => self::$eloq_version,
				'time' => function ($_) {return time();},
				'reimp' => function ($f) {return function (...$args) use($f) { return array_reduce($args, function($f, $a) {return $f($a);}, $f); };},
				'implementation' => self::funConv(2, [$this, 'funConv']),
				'toString' => self::funConv(1, [$this, 'toString']),
				'include' => function ($e) { return include($e); },
				'load' => function($e) { return (include($e))([], $this)->nspace; },
				'eval' => function ($code) { return eval($code); },
				'die' => function ($e) { die(substr(is_string($e) ? $e : $this->toString($e), 0, 10000000)); },
				'echon' => function ($e) { echo substr(is_string($e) ? $e : $this->toString($e), 0, 10000000); try{ob_flush();} catch(Exception $xx) {} return $e; },
				'echo' => function ($e) { echo substr(is_string($e) ? $e : $this->toString($e), 0, 10000000), "\n"; try{ob_flush();} catch(Exception $xx) {} return $e; },
				'preg_match' => self::funConv(2, function($patt, $str) {preg_match($patt, $str, $m); return $m;}),
				'throw' => function($e) {throw new Exception($e);},
				'rule' => function($parser) {return function($rule) use($parser) { return $parser::rule($rule, $parser); };},
				'parse' => function($parser) {return function($rule) use($parser) { return $parser::parse($parser::rule($rule, $parser)); };},
				'parseCatch' => function($parser) {
					return function($rule) use($parser) {
						$pp = $parser::parse($parser::rule($rule, $parser));
						return function ($data) use($pp) {
							try {
								return ["success", $pp($data)];
							} catch(\Throwable $e) {
								$all = [$e->getMessage()];
								$dex[] = [$e->getLine(), $e->getFile()];
								$dex[] = [$e->getTrace()[0]["line"], $e->getTrace()[0]["file"]];
								while($e = $e->getPrevious()) {
									$all[] = $e->getMessage();
									$dex[] = [$e->getLine(), $e->getFile()];
								}
								return ["error", [array_reverse($all), array_reverse($dex)]];
							}
						};
					};
				},
				'_bootstrap_' => self::$parent ? self::$parent->nspace : null,
				'Dispatcher' => function($name) { return function ($argc) use($name) {return new class($argc, self::$nil, $this, $name) {
					private $argc = 0, $classes = [], $nil = null, $par = null, $name = "", $args = [];
					function __construct($argc, $nil, $par, $name, $classes = [], $args = []) {
						$this->argc = $argc;
						$this->nil = $nil;
						$this->par = $par;
						$this->name = $name;
						$this->classes = $classes;
						$this->args = $args;
					}
					function addType($f) {
						$this->classes[] = $f;
					}
					function addClass($f) {
						$this->classes[] = $f;
					}
					function __invoke($x, $composing = false) {

						if ($this->argc > 1)
							return new $this(
								$this->argc - 1,
								$this->nil,
								$this->par,
								$this->name,
								array_map(function ($f) use($x) {return $f($x);}, $this->classes),
								array_merge($this->args, [$x])
							);

						foreach ($this->classes as $f) try {
							$a = $f($x);
							if ( $a != $this->nil )
								return $composing ? $a : $a->args[0];
						} catch (DispatchException $e) {}

						if (!$composing)
							throw new \DispatchException(
								"Missing ''" . $this->name . "'' typeClass implementation for type : " .
								$this->par->toString(array_merge($this->args, [$x]))
							);

						return $this->nil;
					}
				};};},
			];
		}

		static function toBool($e) {
			return $e === self::$nil ? false : $e;
		}

		static function just($a) {
			return self::getStructure('Data', 0, ':>', [$a, self::$nil]);
		}

		static function cons($a, $b) {
			return self::getStructure('Data', 0, ':>', [$a, $b]);
		}

		static function getStructure($kind, $arity, $name, $args) {

			if ( !$args ) {
				$k = "$arity:$kind:$name";
				if (!empty(self::$singletons[$k]))
					return self::$singletons[$k];
			}

			if ( self::$struc )
				$a = new (self::$struc)($kind, $arity, $name, $args);

			else $a = (new class ($kind, $arity, $name, $args) {

				public $kind = "", $arity, $name, $args;

				public function __construct ($kind, $arity, $name, $args = []) {
					$this->kind = $kind;
					$this->arity = $arity;
					$this->name = $name;
					$this->args = $args;
				}

				function __invoke ( $arg ) {
					if ( $this->kind === 'UVar' )
						return new static($this->kind, $this->arity + 1, $this->name, array_merge($this->args, [$arg]));

					if ( $this->arity )
						return new static($this->kind, $this->arity - 1, $this->name, array_merge($this->args, [$arg]));
					throw new \Exception("Can't apply {$this->kind} constructor \"{$this->name}\" anymore, arity exceeded !");
				}

			});

			if ( !$args )
				self::$singletons[$k] = $a;
			return $a;

		}

		{(#str("Mod.toString.php"))}

	});

};
