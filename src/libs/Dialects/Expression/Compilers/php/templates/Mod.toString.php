// <?php


static $t = "    ";


function toStringList ( $data, $ind, $visited ) {
	if ( array_reduce($visited, function($a, $v) use($data){return $a || $v ===$data;}, false) )
		return ["<RECURSION>"];
	$visited[] = $data;

	switch(true) {
		case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
			return array_merge([$this->toString($data->args[0], $ind, $visited)], $this->toStringList($data->args[1], $ind, $visited));
		case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
			return [];
		default:
			return ["<INCORRECT LIST WITH ELEMS>" . $this->toString($data, $ind, $visited) . "</INCORRECT LIST WITH ELEMS>"];
	}
}


function toString ( $data, $ind = "", $visited = [] ) {
	$t = self::$t;

	if ( array_reduce($visited, function($a, $v) use($t, $data){return $a || $v ===$data;}, false) )
		return "<RECURSION>";
	$visited[] = $data;

	switch ( true ) {

		case is_null($data):
			return "null";

		case is_bool($data):
			return $data ? "true" : "false";

		case is_numeric($data):
			return "$data";

		case is_string($data):
			return '"' . str_replace(['\\', '"'], ['\\\\', '\\"'], $data) . '"';

		case is_array($data) && !$data:
			return "[]";

		case is_array($data) && array_reduce(array_keys($data), function($z, $e) { return $z || is_string($e); }, false):
			return "{" . self::toStringMulti(0, $ind, array_map(
				function ( $k, $v ) use ($t, $ind, $visited) { return "$k: " . $this->toString($v, "$ind$t", $visited); },
				array_keys($data),
				$data
			)) . "}";

		case is_array($data):
			return "[" . self::toStringMulti(0, $ind, array_map(
				function ($v, $k) use ($t, $ind, $visited) { return $this->toString($v, "$ind$t", $visited); },
				$data,
				array_keys($data),
			)) . "]";

		case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === ":>":
			array_pop($visited);
			$rrr = $this->toStringList($data, "$ind$t", $visited);
			$rrt = implode(", ",  $rrr);
			return "(" . self::toStringMulti(0, $ind, $rrr) . (count($rrr) === 1 ? ',' : '') . ")";
			if ( strlen($rrt) > 110 )
				return "(\n$ind$t" . implode(",\n$ind$t", $rrr) . "\n$ind)";
			return "(" . $rrt . ")";

		case $data instanceof self::$structure && $data->kind === 'Data' && $data->name === "()":
			return '()';

		case $data instanceof self::$structure && $data->kind === 'Data':
			if ( !$data->args )
				return "{$data->name}";
			return "({$data->name} " . self::toStringMulti(1, $ind, array_map(
				function ($e) use ($t, $ind, $visited) { return $this->toString($e, "$ind$t", $visited); },
				$data->args
			)) . ")";

		case $data instanceof self::$structure && $data->kind === 'Type':
			if ( !$data->args )
				return "#{$data->name}";
			return "(#{$data->name} " . self::toStringMulti(1, $ind, array_map(
				function ($e) use ($t, $ind, $visited) { return $this->toString($e, "$ind$t", $visited); },
				$data->args
			)) . ")";

		case $data instanceof self::$structure && $data->kind === 'Name':
			if ( !$data->args )
				return "@{$data->name}";
			return "(@{$data->name} " . self::toStringMulti(1, $ind, array_map(
				function ($e) use ($t, $ind, $visited) { return $this->toString($e, "$ind$t", $visited); },
				$data->args
			)) . ")";


		case $data instanceof self::$structure && $data->kind === 'UVar':
			if (!$data->args)
				return "\${$data->name}";
			return "\${$data->name}(" . self::toStringMulti(0, $ind, array_map(
				function ($e) use ($t, $ind, $visited) { return $this->toString($e, "$ind$t", $visited); },
				$data->args
			)) . ")";

		 case $data instanceof \Closure:
			$clo = new ReflectionFunction($data);
			$params = implode(", ", array_map(function($a) {return $a->name;}, $clo->getParameters()));
			$closeds = implode(', ', array_keys($clo->getStaticVariables()));
			return "(# \\$params -> <CLOSURE>, [$closeds])";

		default: return sprintf('#%1$s%2$s', get_class($data), self::toString((array) $data, "$ind", $visited));

	}
}

function toStringMulti ($delta, $ind, $x) {
	$t = self::$t;
	$base = "";
	for($i = 0; strlen(trim($base)) < 45 && ( array_key_exists($i, $x) && strlen($x[$i]) ) && strlen(preg_replace("/\n($t)*+/", " ", $x[$i])) < 40; $i++)
		$base .= (strlen($base) ? ($delta ? " " : ", ") : "") . trim(preg_replace("/\n($t)*+/", " ", $x[$i]));
	if ( count($x) == $i )
		return $base;
	if ( count($x) == $i + $delta )
		return (strlen($base) ? "{$base} " : "") . preg_replace("/\n($t)/", "\n", $x[$i]) . " ";
	if ( $delta )
		return "{$base}\n$ind$t" . implode("\n$ind$t", array_slice($x, $i)) . "\n$ind";
	return "\n$ind$t" . implode("\n$ind$t", $x) . "\n$ind";
}
