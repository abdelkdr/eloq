#/**************************************************/
#// Functional Programming Language Module
#/**************************************************/
\OBJECT -> {
	#/********************************/
	#// Parser Helpers
	#/********************************/
	interpolate	= \state rpath -> {
		rpath[0]		-> rpath :
		"/"			-> rpath
		":" / A.in "root" state	-> "{state.root}/{S.rest rpath 1}"
		":" / A.in "dir" state	-> "{state.dir}/{S.rest rpath 1}"
		_ / S.substring 0 8 rpath == "https://" -> rpath
		_ / A.in "dir" state	-> "{state.dir}/{rpath}"
		_ / A.in "root" state	-> "{state.root}/{rpath}"
	}
	pushState	= \state fpath -> {
		file		= fpath
		dir		= Path.dirname file
		fileStack	= (A.in "fileStack" state ? state.fileStack : []) +++ [file]
		root		= A.in "root" state ? state.root : A.in "dir" state ? state.dir : Path.dirname file
	}
	getContext	= \state path
			-> {file = interpolate state path}
			-> {pushState state file, Path.File.getContent file}

	#/**************************************************/
	#// Types
	#/**************************************************/
	TPatt		= PattF TPatt
	TNameF b	= NameF (TNameF b) b
	TName		= TNameF TVar
	TCons		= TNameF TExpr
	TExpr		= ExprF TExpr TPatt

	#/**************************************************/
	#// Data Constructors
	#/**************************************************/
	StatementF a :=
	|	Imp a $p $x
	|	Syn String (TNameF TVar) a
	|	Dec String (TNameF TVar) (*TNameF a)
	|	Aff String String a
	|	Inst $p a a

	ExprF a p o :=
	|	Ide a
	|	Mod a
	|	ModImp a
	|	ModRef String
	|	Emb a
	|	Obj o
	|	Nfo $x a
	#/*** Implementation Specific ***/
	|	Imt String (*a)
	#/*** Bases Types ***/
	|	Str String
	|	Int Integer
	|	Tpl String (*a)
	#/*** Lambda Calculus ***/
	|	Cmp a a
	|	App a a
	|	Var String
	|	Lam String a
	|	Pi (?String) a a
	|	Sco (*StatementF a) (?a)
	#/*** Lists ***/
	|	Nil
	|	Car a
	|	Cdr a
	|	Cns a a
	#/*** Algebraic Data Types ***/
	|	Uar a
	|	Una a
	|	Jdg a a
	|	TEq a a
	|	UVar String (*a)
	|	Match a (*{*patt: p, gard: (?a), expr: a})
	|	Dat Integer String (*a)
	|	Typ Integer String (*a)
	|	Nam Integer String (*a)
	#/*** Imperative Data Structures ***/
	|	Upd a (*a) a
	|	Acc a a
	|	Tup (*a)
	|	Arr (*a)
	|	Map (*{*a, a})
	|	Rcd (*{*a, a})
	#/*** Ops ***/
	|	TOp a a a
	|	UOp String a
	|	BOp String a a
	|	Cpr String a a
	|	Coa String a a

	TVar :=
	|	Var String

	NameF a b :=
	|	App a b
	|	Var String

	PattF a :=
	|	Var String
	|	Str String
	|	Int Integer
	|	App a a
	|	Nil
	|	Cns a a
	|	Dat Integer String (*a)
	|	Typ Integer String (*a)
	|	Nam Integer String (*a)
	|	Tup (*a)
	|	Arr (*a)
	|	Map (*{*a, a})
	|	Rcd (*{*a, a})

	#/**************************************************/
	#// Helpers for Data Constructors
	#/**************************************************/
	mkBranch	= \ps g e -> {patt: Tup ps, gard: g, expr: e}
	mkMatch		= \values branches default -> Match (Tup values) (L.concat branches (M.fmap (mkBranch () ()) default))

	#/**************************************************/
	#// Components
	#/**************************************************/
	Rho		= Env {key: (.0), value: (.1)}
	Parser		= (#exp("Expression.Parser"))
	Base		= (#exp("Expression.Base"))
	Classes		= Base +++ Generics (GGenerics Base.gen)
	Names		= (#exp("Aspects/Names"))

	simplify	= \ast -> {
		ast			-> ast :
		Tpl "" as		-> Str ""
		Tpl s ()		-> Str $ S.sprintf s []
		Nfo i (Nfo j a)		-> simplify $ Nfo j a
		Nfo i (App f a)		-> Nfo i $ simplify $ App f a
		Nfo i a			-> Nfo i $ simplify a
		App (Nfo i f) a		-> Nfo i $ simplify App f a
		App (Nam i n as) a / i 	-> Nam (i - 1) n $ L.concat as (a,)
		App (Typ i n as) a / i	-> Typ (i - 1) n $ L.concat as (a,)
		App (Dat i n as) a / i	-> Dat (i - 1) n $ L.concat as (a,)
		App (UVar n as) a	-> UVar n $ L.concat as (a,)
	}

	simplifyRec	= simplify . Classes.fmap simplifyRec . simplify

	#/**************************************************/
	#// Interfaces
	#/**************************************************/
	rdr		= ((#exp("Aspects/Read")))
	read		= \xdata -> (rdr xdata).reader
	reader		= \xdata -> (rdr xdata)
	serializer	= (#exp("Aspects/Serialize"))
	compiler	= (#exp("Compilers/php/Compiler"))

}
