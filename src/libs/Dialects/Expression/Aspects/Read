\xdata -> {

	flip	= \f a b -> f b a
	reader	=  \data ast -> (read data ast q.ok (.0) {modules: [], stacked: [], i: 0})
	readerM	=  \data ast -> (readModule data ast q.ok (.0) {modules: [], stacked: [], i: 0})

	q	= {
		ok	= \z ->		\p st -> p {z, st}
		exMod	= \file j ->	\p st -> j (file `A.in` st.modules) p st
		getMod	= \file j ->	\p st -> j st.modules[file] p st
		getMods	= \j ->		\p st -> j st.modules p st
		fresh	= \v j ->	\p st -> j "{v}_{st.i}" p st[i:= st.i + 1]
		setMod	= \file m ->	\p -> p . \st -> st[modules, (file):= m]
		getFil	= \file j ->	j $ implementation 1 "file_get_contents" file
		setFil	= \file s ->	implementation 2 "file_put_content" file s
		stack	= \file ->	\p st -> p st[stack, (file):= 1]
		unstack	= \file ->	\p st -> p st[stack:= st.stack --- [(file): 1]]
		stacked = \file j ->	\p st -> j (file `A.in` st.stack) p st
	}

	last	= \data res
			-> data[scope:= A.foldr (\a i -> i[a.0:= Var a.0]) data.scope res.1.modules] @ \data
			-> flip (algebra data) (Sco (A.foldr (\mod i -> Aff mod.0 mod.0 mod.1 :> i) () res.1.modules) (res.0,)) \nsco
			-> flip (algebra data) (Mod nsco) ($)

	readRootFile	= \data file -> readModule data file q.ok ($) {stack: [], modules: [], i: 0} @ last data

	readRootText	= \data text -> xdata.parser "Module2" [data.context, text] @ \ast
			-> read data ast q.ok ($) {stack: [], modules: [], i: 0} @ last data


	zipRead	= \coast codata ok -> Classes.zip read codata coast @ Classes.gen ($) ok

	read	= \data ast ok -> coalg data ast \coast -> {
		coast			-> zipRead coast (codata data coast) (algebra data ok) :
		ModRef x		-> readModule data (x.path[0] == "/" ? x.path : "{x.ctx.dir}/{x.path}") ok
		Sco stms (e,)		-> q.fresh "tc_inst_id" \tcid
					-> Names.Scopes.scopeIn data.scope stms @ \scoIn
					-> L.fmap (\: x -> x : Inst p g y
						-> (redo 0 tcid () () ($) (subst scoIn p) @ \x
						-> Inst x.0 (g == () ? () : (Int 1,)) (x.1 g y))
					 ) stms @ \stms
					-> zipRead (Sco stms (e,)) (codata data $ Sco stms (e,)) (algebra data ok)
	}

	redo	= \n id xs vs f:
		App l r		-> redo (n+1) id (r :> xs) ("lamtc_{id}_{n}" :> vs) f l
		Nfo i a		-> redo n id xs vs (Nfo i . f) a
		x		-> {x, \gard body -> L.foldr Lam (Match (Tup $ L.fmap Var vs) (
					{patt: Tup xs, gard: gard, expr: (Cns body Nil)}
					{patt: Tup $ L.fmap Var vs, gard: (), expr: Nil}
				,)) vs, f}

	readModule	= \data file ok -> q.exMod file \exist -> {
		exist ? 1 : 0 :
		1		-> q.getMod file \mod -> ok mod.2
		0		-> q.stacked file \stacked -> {stacked ? 1 : 0 :
		1		-> die "circular module dependency in file : '{file}'"
		0		-> q.stack file . q.fresh "___module___" \m
				-> q.setMod file {m, (), {frec: Var m, ast: Var m, info: []}} . q.getFil file \content
				#//-> q.getFil file \content
				-> xdata.parser "Module2" [[
					root: Path.dirname file
					dir: Path.dirname file
					fileStack: data.context.fileStack +++ [file]
					file: file
				], content] @ \ast
				-> read data ast \res
				-> q.unstack file
				.  q.setMod file {m, res, {frec: Var m, ast: Var m, info: []}} .  ok {frec: Var m, ast: Var m, info: []} }
	}

	#//**************************************************/
	#// Internals
	#//**************************************************/
	coalg	= \data ast ok -> {
		Names.init ast :
		Sco stmts ()		-> ok $ simplify $ Sco stmts $ (Names.Scopes.buildRcd stmts,)
		Obj o			-> ok $ simplify $ Obj $ OBJECT.read xdata data o
		Match e brs		-> ok $ simplify $ Match e $ L.mapf brs \br -> br +++ {patt: simplifyRec $ subst data.scope br.patt}
		fast			-> ok $ simplify $ fast
	}

	subst	= \scope ast -> simplifyRec {
		{
			ast			-> simplify $ Classes.fmap (subst scope) ast :
			Var v / A.in v scope	-> scope[v]
		} :
		App x a -> {
			x			-> simplify $ App x a :
			Dat i n as		-> Dat (i - 1) n $ L.concat as (a,)
			Typ i n as		-> Typ (i - 1) n $ L.concat as (a,)
			Nam i n as		-> Nam (i - 1) n $ L.concat as (a,)
			UVar n as		-> UVar n $ L.concat as (a,)
		}
		x -> x
	}

	codata	= \data ast -> Classes.zipo {
		context	= Classes.place ("context" `A.in` data ? data.context : []) ast
		path	= Names.path data.path ast
		scope	= Names.scope data.scope ast
		#//fscope	= Names.fscope ("fscope" `A.in` data ? data.fscope : data.scope) ast
		ctx	= Names.location data.ctx ast
		nfo	= {
			ast	-> Classes.place data.nfo ast :
			Nfo i a	-> Nfo i i
		}
	}

	algebra	= \data ok frec -> ok {
		frec	-> {
				frec@_	= frec
				info	= {
					frec :
					Var v / !A.in v data.scope	->
						["In file '{data.nfo.file}':{data.nfo.line}:{data.nfo.col}, Undefined variable \"{v}\" in context {data.ctx}"]
					ast				-> Classes.fold ((+++) . (.info)) [] frec
				}
				ast	= simplify {
					Classes.fmap (.ast) frec :
					Var v / A.in v data.scope	-> data.scope[v]
					ast				-> ast
				}
			} :
		ModImp a-> a
		Obj o	-> {
				frec@_	= frec
				info	= L.fold ((+++) . (.info)) o.2 o.1
				ast	= Obj o.0
			}
	}


















}


%%


\xdata -> {
	#//**************************************************/
	#// Names initialisation
	#//**************************************************/
	read	= \x -> Classes.hylo algebra codata coalg read x
	read	= \data -> algebra data . (\ast -> Classes.zip read (codata data ast) ast) . coalg data

	#//**************************************************/
	#// Internals
	#//**************************************************/
	coalg	= \data ast -> simplify {
		Names.init ast :
		Sco stmts ()		-> Sco stmts $ (Names.Scopes.buildRcd stmts,)
		Obj o			-> Obj $ OBJECT.read xdata data o
		Match e brs		-> Match e $ L.mapf brs \br -> br +++ {patt: (subst data.scope br.patt)}
		fast			-> fast
	}

	subst	= \scope ast -> {
		{
			ast			-> Classes.fmap (subst scope) ast :
			Var v / A.in v scope	-> scope[v]
		} :
		App x a -> {
			x			-> App x a :
			Dat i n as		-> Dat (i - 1) n $ L.concat as (a,)
			Typ i n as		-> Typ (i - 1) n $ L.concat as (a,)
			Nam i n as		-> Nam (i - 1) n $ L.concat as (a,)
		}
		x -> x
	}

	codata	= \data ast -> Classes.zipo {
		path	= Names.path data.path ast
		scope	= Names.scope data.scope ast
		ctx	= Names.location data.ctx ast
		nfo	= {
			ast	-> Classes.place data.nfo ast :
			Nfo i a	-> Nfo i i
		}
	}

	algebra	= \data frec -> {
		frec	-> {
				frec@_	= frec
				info	= {
					frec :
					Var v / !A.in v data.scope	-> echo data.nfo ??
						["In file '{data.nfo.file}':{data.nfo.line}:{data.nfo.col}, Undefined variable \"{v}\" in context {data.ctx}"]
					ast				-> Classes.fold ((+++) . (.info)) [] frec
				}
				ast	= simplify {
					Classes.fmap (.ast) frec :
					Var v / A.in v data.scope	-> data.scope[v]
					ast				-> ast
				}
			} :
		Obj o	-> {
				frec@_	= frec
				info	= L.fold ((+++) . (.info)) o.2 o.1
				ast	= Obj o.0
			}
	}


}.read



%%


















@from "path/tothe/file.ext" import Module;
@from "path/tothe/file.ext" import {Var, Var, Var, Var : ReName};






















