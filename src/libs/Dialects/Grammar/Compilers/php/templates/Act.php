// START ACTION <?php
$node = (function ($oldState){mkUse $ Sig.addL fa.free ("state", "context")} {
	{indents 1 a}
	if ($node) try {
		$node = {b ? "" : "["}{indents 1 e}(
			self::$expressionContext[0],
			array_merge(self::$expressionContext[1], [
				'this' => $node[0],
				'context' => array_merge($context, [
					'state' => $state,
					'oldState' => $oldState,
				]),
			])
		){b ? "" : "]"};
	} catch(\Throwable $e) {
		throw new \Exception("{fdata @ \(Act n {ast: Emb (Nfo info _)} _) -> S.escape "Semantics Action Error on line {info.line} in file '{info.file}'"}", 0, $e);
	}
	return $node;
})($state);
// END ACTION
