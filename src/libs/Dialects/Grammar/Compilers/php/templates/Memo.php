// START MEMO <?php
${keyVar} = "\{$state['ignore']}\{$state['pos']}\{$state['precedence'][0]}\{$state['precedence'][1]}{S.escape keyId}\{$parser->parserId}";
if ( !empty($context['memo'][${keyVar}]) ) {
	$mem = $context['memo'][${keyVar}];
	if ( $mem[1] ) {
		$state['input'] = $mem[0]['input'];
		$state['pos'] = $mem[0]['pos'];
		$state['line'] = $mem[0]['line'];
		$state['col'] = $mem[0]['col'];
		$state['precedence'] = $mem[0]['precedence'];
	}
	$node = $mem[1];
} else {
	// START MEMO_BODY
	{indents 1 body}
	// END MEMO_BODY
	$context['memo'][${keyVar}] = [$state, $node];
}
// END MEMO
