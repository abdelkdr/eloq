
indexes	= L.nums 9
symbols	= (L.nums 10).$$

Serial	= {
	
	isHole		= \: (x,) -> 0, _ -> 1
	Board		= {tot: 0, name: "", grid: [], sols: ()}
	
	
	clean	= \b -> 0 ? [] : b --- [grid: 1, name: 1, sols: 1, auto_skip: 1, skip: 1, pos: 1]
	write	= \:
		"all"		-> \b -> L.foldr (write "solu") (write "grid" b.grid) b.sols
		"solu"		-> \solu out -> S.explode "\n" out @ A.zip (\x z -> "{z}\t{x}") (S.explode "\n" $ write "grid" solu) @ A.implode "\n"
		"file"		-> \boards -> A.fmap (write "board") boards @ A.implode "\n\n"
		"board"		-> \b -> "{S.repeat '#' 100}\n# {b.name}\n{write "head" b}{S.repeat '#' 100}{write "all" b}"
		"head"		-> \b -> A.keys (clean b) @ A.fmap (\k -> "# {S.align ' ' 25 k}: {S.json.encode b[k]}\n") @ A.implode ""
		"grid"		-> {show = \i a -> i >= 81 ? "" : "{isHole a[i] ? '.' : a[i].$}{!((i + 1) % 9) ? "\n" : ' '}{show (i+1) a}"} -> \a -> "\n{show 0 a}"
	;
	
	read = (\parse rule str -> parse rule [[], str]) $ {:
		
		file		-> main { "%%" /\s++/ skip }.2? /\s++/? /$/			=> [ this.0 +++ $ this.1 ? this.1.$ : [] ]
		main		-> board* /\s++/?						=> [ this.0 @ flip L.foldl [] \i s -> i[s.name:= s] ]
		skip		-> main								=> [ A.mapf this \b -> b[auto_skip:= 1] ]
		
		board		-> { header "\n" }.0+ grid $flush				=> [ L.foldl (+++) Board this.0 +++ [grid: this.1] ]
		header		-> "# GRID " /.++/.0						=> [ [name: S.trim "GRID {this.1}"] ]
				-> "# solver_" /[a-zA-Z_0-9]++/ /\s++/? /\:|\|/ /.++/		=> [ ["solver_{S.trim this.1.0}": S.json.decode $ S.trim this.4.0 ] ]
				-> "# " /[a-zA-Z_0-9]++/ /\s++/? /\:|\|/ /.++/			=> [ [S.trim this.1.0: S.trim this.4.0 ] @ \x -> S.trim this.1.0 `A.in` ["tries":1, "tot":1] ? [S.trim this.1.0: S.parseInt this.4.0 ] : x]
				-> /(\#| |\t)++/?						=> [ [] ]
		
		
		grid		-> three(three(row)) /\s++/?					=> [ S.explode " " this.0 @ A.fmap \x -> x == "." ? symbols : (S.parseInt x,) ]
		three(X)	-> X X X							=> [ "{this.0} {this.1} {this.2}" ]
		row		-> { /\s++/? /(?:[1-9\.] ){8}[1-9\.]/.0 /.++/? ("\n" | /$/) }.1
		
	}
	
	
	writeq	= \C -> "{C}{C}{C}\n{C}{C}{C}\n{C}{C}{C}"
	writeq	= \C -> "---\n-{C}-\n---"
	bigs	= [
		"╔═╗\n║.║\n╚═╝", 
		".║.\n.║.\n.║.", 
		"══╗\n╔═╝\n╚══", 
		"══╗\n.═╣\n══╝", 
		"║.║\n╚═╣\n..║", 
		"╔══\n╚═╗\n══╝",
		"╔══\n╠═╗\n╚═╝",
		"══╗\n..║\n..║",
		"╔═╗\n╠═╣\n╚═╝", 
		"╔═╗\n╚═╣\n══╝",
	]
	
	writen	= \grid -> (\x -> "{L.implode " " x.1}\n{x.2}") $ A.fmap (\x -> isHole x ? "." : x.$) grid @ flip A.foldr {0, (), ""} \c i 
		-> (!i.0 ?: i.0 % 9) ? {i.0 + 1, c :> i.1, i.2} : {i.0 + 1, (c,), "{L.implode " " i.1}\n{i.2}"}
		
	writec	= \C -> bigs[C.$]
	writeh	= \H -> H == () ? bigs[0] : L.foldlk symbols "" \i s -> "{i}{s `L.in` H ? s : "-"}{(s % 3 == 0 ?? s != 9) ? "\n" : ""}"
	writes	= \grid -> A.fmap (\x -> isHole x ? writeh x : writec x) grid @ (\x -> S.trim "{x.1}\n\n{x.2}") . flip A.foldl {0, "", ""} \i c 
			-> (!i.0 ?: i.0 % 9) ? 
				{i.0 + 1, i.1, !i.2 ? c : $ S.explode "\n" i.2 @ \z 
					-> S.explode "\n" c @ A.zip (\a b -> "{a}{i.0 % 3 ? " " : "   "}{b}") z @ \x -> A.implode "\n" x } :
				{i.0 + 1, "{i.1}\n\n{(i.0 + 18) % 27 ? "" : "\n\n"}{i.2}", c}
	
	
}

 


grid0 = "
8 . . . . . . . .
. . 3 6 . . . . .
. 7 . . 9 . 2 . .
. 5 . . . 7 . . .
. . . . 4 5 7 . .
. . . 1 . . . 3 .
. . 1 . . . . 6 8
. . 8 5 . . . 1 .
. 9 . . . . 4 . .
"

grid1 = "
. . . . . . . . .
1 . . . 9 5 . . .
. 9 8 . . . . 6 .
8 . . . 6 . . . 3
4 . . 8 . 3 . . 1
7 . . . 2 . . . 6
. 6 . . . . 2 8 .
. . . 4 1 9 . . 5
. . . . 8 . . 7 9
"

grid2 = "
# . . . . 8 . . 6
. . . 7 # 2 . . .
. . 9 . 4 . 3 . 1
. 1 # 8 . . . 5 .
9 . . # 2 . . . 4
. 7 . . . 9 . 8 #
7 # 6 . 3 . 5 . .
. . . 1 . 7 # . .
3 . . 2 . # . . .
"

grid3 = "
9 . 6 . 3 7 . . 2
. . 5 . . . . 6 .
3 1 . 4 . . . . .
. . 9 . 5 . . . .
. . . 8 . 1 . . .
. . . . 4 . 7 . .
. . . . . 6 . 7 9
. 5 . . . . 1 . .
2 . . 1 7 . 6 . 3
"
