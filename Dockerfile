FROM php as dev

RUN cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini
RUN sed -i 's/memory_limit = 128M/memory_limit = 4G/g' /usr/local/etc/php/php.ini

FROM dev as prod
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY . ./
CMD ["php", "tests/bootstrap.php"]
