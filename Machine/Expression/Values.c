/*******************************************************/
// GETSIZE
/*******************************************************/
char* Value_getName(Value this) {
	SWITCH(this) {
		CASE(Int): return "Int";
		CASE(Nil): return "Nil";
		CASE(Cns): return "Cns";
		default: printf("\n\nERROR : Value_getName not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// GETSIZE
/*******************************************************/
nat64 Value_getSize(Value this) {
	switch(this->tag) {
		case ValueTag_Nil: return sizeof(S_Value_Nil);
		case ValueTag_Cns: return sizeof(S_Value_Cns);
		case ValueTag_Tup: return sizeof(S_Value_Tup) + sizeof(Value[this->Tup.len]);
		//~ CASE(Kont): return sizeof(S_Value_Kont) + sizeof(Value[this->Kont.size]) + 0;
		//~ CASE(Closure): return sizeof(S_Value_Closure) + sizeof(Value[this->Closure.size]) + 0;
		//~ CASE(Boo): return sizeof(S_Value_Boo) + 0;
		//~ CASE(Nat64): return sizeof(S_Value_Nat64) + 0;
		//~ CASE(Nil): return sizeof(S_Value_Nil);
		//~ CASE(Cons): return sizeof(S_Value_Cons);
		//~ CASE(Zer): return sizeof(S_Value_Zer) + 0;
		//~ CASE(Suc): return sizeof(S_Value_Suc) + 0;
		//~ CASE(Num): return sizeof(S_Value_Num) + 0;
		//~ DEFERROR(getSize, this); return 0;
		default: printf("Value_getSize not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// NEW
/*******************************************************/
Value Value_new(Value this) {
	Value new = malloc(Value_getSize(this));
	//~ printf("size: %lu => %lu, new : %p, tup: %p \n", sizeof(new->Tup), sizeof(S_Value_Tup), new, &(new->Tup) + 1);
	switch(this->tag) {
		case ValueTag_Cns: new->Cns = this->Cns; return new;
		case ValueTag_Tup: new->Tup = this->Tup; new->Tup.tup = (Value*) (&(new->Tup) + 1); return new;
		default: printf("Value_new not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// COPY
/*******************************************************/
Value Value_copy(Value src, Value tgt) {
	switch(src->tag) {
		//~ case ValueTag_Nil: tgt->Nil = src->Nil;	return tgt;
		//~ case ValueTag_Cns: return sizeof(S_Value_Cns);
		//~ case ValueTag_Tup: return sizeof(S_Value_Tup) + sizeof(Value[this->Tup.len]);
		default: printf("Value_copy not implemented for some type !\n"); exit(1);
	}
}

Value* Value_Iterator_init(Value this) {
	switch(this->tag) {
		case ValueTag_Cns: return &(this->Cns.car);
		case ValueTag_Tup: return this->Tup.tup;
		default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	}
}

Value* Value_Iterator_next(Value this, Value* it) {
	switch(this->tag) {
		case ValueTag_Cns: return it + 1;
		case ValueTag_Tup: return it + 1;
		default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	}
}

//~ Value* Value_Iterator_valid(Value this, Value* it) {
	//~ switch(this->tag) {
		//~ case ValueTag_Cns: return it + 1;
		//~ case ValueTag_Tup: return it + 1;
		//~ default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	//~ }
//~ }


/*******************************************************/
// DUMP
/*******************************************************/
void Value_dump(Value this, nat64 i) {
	printf("# %lu %s	%p %s	", i, i < 100000 ? "\t" : "", this, this < (Value) 0x100000 ? "\t" : "");
	if (this) {
		printf(
			"%s(size:%lu){",
			Value_getName(this),
			Value_getSize(this)
		);
		switch(this->tag) {
			CASE(Nil):
				break;
			CASE(Cns):
				printf("car: %p, ", this->Cns.car);
				printf("cdr: %p", this->Cns.cdr);
				break;

			default: printf("Value_dump not implemented for some type !\n"); exit(1);
		}
		printf("}\n");
	} else printf("# EMPTY VALUE\n");
}




void ListInt_print(Value this) {
	printf("(");
	for(;;) {
		printf("%li", this->Cns.car->Int.val);
		this = this->Cns.cdr;
		if (this->tag == ValueTag_Cns)
			printf(", ");
		else break;
	}
	printf(")");
}
