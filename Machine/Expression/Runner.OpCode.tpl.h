//**************************************/
// OpCodes
//**************************************/
typedef enum {
	{L.implode ",\n\t" $ opCodes @ L.fmap \opcode -> "XOpCode_{opcode}"}
} XOpCode;

char* XOpCode_Names[] = {
	{L.implode ",\n\t" $ opCodes @ L.fmap \opcode -> "\"{opcode}\""}
};

//****************************************************************************/
// Instructions
//****************************************************************************/
typedef struct {int32 addr; nat32 size;} Cljr;
typedef union U_XInstructionData XInstructionData;
union U_XInstructionData {
	nat32		vrbl;
	int32		addr;
	int64		size;
	int64		nmbr;
	Cljr		cljr;
	Value*		valu;
	Value*		patt;
	Value		(*uope)(Value);
	Value		(*bope)(Value, Value);
};

typedef struct S_XInstruction* XInstruction;
typedef struct S_XInstruction S_XInstruction;
struct S_XInstruction {
	XOpCode			cod;
	XInstructionData	dat;
};
