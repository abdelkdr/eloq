//~ #define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

typedef bool boo;
typedef int32_t int32;
typedef int64_t int64;
typedef uint32_t nat32;
typedef uint64_t nat64;

//~ #include "Values.Macro.h"
#define SWITCH(v)			switch(v->tag)
#define CASE(Tag)			case ValueTag_##Tag

//~ #define SIZE_Int			sizeof(S_Value_Int)
//~ #define SIZE_Nil			sizeof(S_Value_Nil)
//~ #define SIZE_Cns			sizeof(S_Value_Cns)
//~ #define SIZE_Tup(len)			(sizeof(S_Value_Tup) + sizeof(Value[len]))

//~ #define S_VALUE(Type, ...)		((S_Value_##Type) {ValueTag_##Type, __VA_ARGS__})
//~ #define VALUE(Type, ...)		((Value) &S_VALUE(Type, __VA_ARGS__))

//~ #define VALUE(Tag, ...)			&((S_Value) ({Tag: STRCT(Tag, __VA_ARGS__)}))
#define VALUE(Tag, ...)			((Value) &({Tag: STRCT(Tag, __VA_ARGS__)}))
#define STRCT(Tag, ...)			((S_Value_##Tag) {tag: ValueTag_##Tag, __VA_ARGS__})


#define MAKE(Tag, ...)			({\
						struct Value_##Tag* value = malloc(sizeof(struct Value_##Tag)); \
						*value = (struct Value_##Tag){ValueTag_##Tag, __VA_ARGS__}; \
						(Value) value; \
					})


#define NEW(Tag, ...)			INIT(ALLOC(Tag, __VA_ARGS__), Tag, __VA_ARGS__)
#define INIT(this, Tag, ...)		({S_Value_##Tag* _this = this; _this->tag = ValueTag_##Tag; INIT_##Tag(_this __VA_OPT__(, __VA_ARGS__)); (Value)_this;})
#define ALLOC(Tag, ...)			malloc(SIZE(Tag, __VA_ARGS__))
#define SIZE(Tag, ...)			(sizeof(S_Value_##Tag) SIZE_##Tag __VA_OPT__((__VA_ARGS__)))

#define INIT_Int(this)
#define INIT_Nil(this)
#define INIT_Cns(this)
#define INIT_Tup(this, param_len)	this->len = param_len; this->tup = (Value*)(this + 1);


#define SIZE_Int
#define SIZE_Nil
#define SIZE_Cns
#define SIZE_Tup(param_len)		+ sizeof(Value[param_len])


typedef union U_Value Value;
//****************************************************************************/
// Instructions
//****************************************************************************/
#include "../AutoGen/Runner.OpCode.h"

//********************************************************/
// VALUES
//********************************************************/



typedef enum ValueTag {
	ValueTag_Non                   = 1,
	ValueTag_Jst                   = 2,
	ValueTag_Jst1u                 = 3,
	ValueTag_Nil                   = 4,
	ValueTag_Cns                   = 5,
	ValueTag_Cns1u                 = 6,
	ValueTag_Boo                   = 7,
	ValueTag_Int                   = 9,
	ValueTag_Var                   = 10,
	ValueTag_Jst1b                 = 8,
	ValueTag_Cns1b                 = 16,
	ValueTag_Cns2                  = 24,
	ValueTag_Str                   = 32,
	ValueTag_Tup                   = 40,
	ValueTag_Arr                   = 48,
	ValueTag_Clo                   = 56,
	ValueTag_Clj                   = 64,
	ValueTag_Kon                   = 72,
} ValueTag;

char* ValueTag_Names(ValueTag t) {
	switch(t) {
		case ValueTag_Non: return "ValueTag_Non";
		case ValueTag_Jst: return "ValueTag_Jst";
		case ValueTag_Jst1u: return "ValueTag_Jst1u";
		case ValueTag_Nil: return "ValueTag_Nil";
		case ValueTag_Cns: return "ValueTag_Cns";
		case ValueTag_Cns1u: return "ValueTag_Cns1u";
		case ValueTag_Boo: return "ValueTag_Boo";
		case ValueTag_Int: return "ValueTag_Int";
		case ValueTag_Var: return "ValueTag_Var";
		case ValueTag_Jst1b: return "ValueTag_Jst1b";
		case ValueTag_Cns1b: return "ValueTag_Cns1b";
		case ValueTag_Cns2: return "ValueTag_Cns2";
		case ValueTag_Str: return "ValueTag_Str";
		case ValueTag_Tup: return "ValueTag_Tup";
		case ValueTag_Arr: return "ValueTag_Arr";
		case ValueTag_Clo: return "ValueTag_Clo";
		case ValueTag_Clj: return "ValueTag_Clj";
		case ValueTag_Kon: return "ValueTag_Kon";
	}
	return "<ValueTag_UNKNOW>";
};

typedef union U_Value Value;
typedef union S_Value S_Value;
typedef Value (*Value_UnaryOp)(Value);
typedef Value (*Value_BinaryOp)(Value, Value);
typedef struct S_Value_Jst1b S_Value_Jst1b;
typedef struct S_Value_Cns1b S_Value_Cns1b;
typedef struct S_Value_Cns2 S_Value_Cns2;
typedef struct S_Value_Str S_Value_Str;
typedef struct S_Value_Tup S_Value_Tup;
typedef struct S_Value_Arr S_Value_Arr;
typedef struct S_Value_Clo S_Value_Clo;
typedef struct S_Value_Clj S_Value_Clj;
typedef struct S_Value_Kon S_Value_Kon;
typedef struct S_Value_Jst1b* Value_Jst1b;
typedef struct S_Value_Cns1b* Value_Cns1b;
typedef struct S_Value_Cns2* Value_Cns2;
typedef struct S_Value_Str* Value_Str;
typedef struct S_Value_Tup* Value_Tup;
typedef struct S_Value_Arr* Value_Arr;
typedef struct S_Value_Clo* Value_Clo;
typedef struct S_Value_Clj* Value_Clj;
typedef struct S_Value_Kon* Value_Kon;
typedef struct S_Value_Non {
	ValueTag                       tag;
} S_Value_Non;
typedef struct S_Value_Jst {
	ValueTag                       tag;
} S_Value_Jst;
typedef struct S_Value_Jst1u {
	ValueTag                       tag;
	S_Value*                       car;
} S_Value_Jst1u;
typedef struct S_Value_Nil {
	ValueTag                       tag;
} S_Value_Nil;
typedef struct S_Value_Cns {
	ValueTag                       tag;
} S_Value_Cns;
typedef struct S_Value_Cns1u {
	ValueTag                       tag;
	S_Value*                       car;
} S_Value_Cns1u;
typedef struct S_Value_Boo {
	ValueTag                       tag;
	boo                            val;
} S_Value_Boo;
typedef struct S_Value_Int {
	ValueTag                       tag;
	int64                          val;
} S_Value_Int;
typedef struct S_Value_Var {
	ValueTag                       tag;
	int64                          var;
} S_Value_Var;
typedef union U_Value {
	nat64                          tagPtr;
	ValueTag                       tag;
	ValueTag*                      ptag;
	S_Value*                       value;
	S_Value_Non                    Non;
	S_Value_Jst                    Jst;
	S_Value_Jst1u                  Jst1u;
	S_Value_Jst1b*                 Jst1b;
	S_Value_Nil                    Nil;
	S_Value_Cns                    Cns;
	S_Value_Cns1u                  Cns1u;
	S_Value_Cns1b*                 Cns1b;
	S_Value_Cns2*                  Cns2;
	S_Value_Boo                    Boo;
	S_Value_Int                    Int;
	S_Value_Var                    Var;
	S_Value_Str*                   Str;
	S_Value_Tup*                   Tup;
	S_Value_Arr*                   Arr;
	S_Value_Clo*                   Clo;
	S_Value_Clj*                   Clj;
	S_Value_Kon*                   Kon;
} U_Value;
typedef struct S_Value_Jst1b {
	ValueTag                       tag;
	Value                          car;
} S_Value_Jst1b;
typedef struct S_Value_Cns1b {
	ValueTag                       tag;
	Value                          car;
} S_Value_Cns1b;
typedef struct S_Value_Cns2 {
	ValueTag                       tag;
	Value                          car;
	Value                          cdr;
} S_Value_Cns2;
typedef struct S_Value_Str {
	ValueTag                       tag;
	nat64                          len;
	char*                          str;
} S_Value_Str;
typedef struct S_Value_Tup {
	ValueTag                       tag;
	nat64                          len;
	Value*                         arr;
} S_Value_Tup;
typedef struct S_Value_Arr {
	ValueTag                       tag;
	nat64                          len;
	Value*                         arr;
} S_Value_Arr;
typedef struct S_Value_Clo {
	ValueTag                       tag;
	XInstruction                   cod;
	nat64                          len;
	Value*                         env;
} S_Value_Clo;
typedef struct S_Value_Clj {
	ValueTag                       tag;
	XInstruction                   cod;
	Value_Arr                      env;
} S_Value_Clj;
typedef struct S_Value_Kon {
	ValueTag                       tag;
	XInstruction                   cod;
	nat32                          len;
	Value*                         env;
} S_Value_Kon;
typedef union S_Value {
	ValueTag                       tag;
	S_Value_Jst1b                  Jst1b;
	S_Value_Cns1b                  Cns1b;
	S_Value_Cns2                   Cns2;
	S_Value_Str                    Str;
	S_Value_Tup                    Tup;
	S_Value_Arr                    Arr;
	S_Value_Clo                    Clo;
	S_Value_Clj                    Clj;
	S_Value_Kon                    Kon;
} S_Value;


char* Value_Names(Value v) {
	if (v.tag & 7)
		return ValueTag_Names(v.tag);
	return ValueTag_Names(*v.ptag);
}
