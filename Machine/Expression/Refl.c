
//**************************************/
// Circular Allocator
//**************************************/
typedef struct CircularPage* CircularPage;
typedef struct CircularPage {
	nat32			stck;
	nat32			size;
	nat32			free;
	CircularPage		prev;
	CircularPage		next;
	Value*			curr;
} S_CircularPage;

int CAllocator_push(CircularPage* p, nat64 size) {

	//~ printf("ALLOCATE PUSH \n");

	if (p[0]->next) {
		*p = p[0]->next;
		return 0;
	}
	else {

		nat64 s = max(size, 2 * max(p[0]->size, p[0]->next ? p[0]->next->size : 0));
		CircularPage q = malloc(sizeof(S_CircularPage) + sizeof(Value) * s);
		//~ printf("ALLOCATE %p \n", q);
		*q = (S_CircularPage) {stck: 0, size: s, free: s, prev: *p, next: NULL, curr: (Value*) (q + 1)};
		p[0]->next = q;
		*p = q;
		return 1;
	}
}

void CAllocator_free(CircularPage p) {
	for(CircularPage q = p->prev; q; p = q, q = q->prev)
		if (!p->stck)
			free(p);
	if (!p->stck)
		free(p);
}

//**************************************/
//
//**************************************/
Value refl(Value l, Value r) {

	CircularPage	head = &(S_CircularPage) {stck: 1, size: 128, free: 126, prev: NULL, next: NULL, curr: ((Value[128]) {l, r}) + 2};
	CircularPage	writ = head;
	CircularPage	read = head;
	Value*		curr = read->curr - 2;
	Value v;

	int u = 0;
	while (curr != writ->curr)
		if (curr == read->curr) {

			//~ printf("NEXT PAGE %p => %p !\n", read, read->next);
			read->curr -= read->size - read->free;
			read->free = read->size;
			read->prev = head;
			head->next = read;
			head = read;
			read = read->next;
			read->prev = NULL;
			head->next = NULL;

			//~ printf("NEXT PAGE %p !\n", read);
			curr = read->curr - read->size + read->free;
			//~ printf("NEXT PAGE %p read->curr : %p, curr : %p => %p, diff : %li !\n", read, read->curr, curr, curr + 2, read->curr - curr);

		} else while (curr != read->curr) {

			u++;
			//~ printf("PROGRESS %i %u !\n", u++, writ->free);
			l = curr[0];
			r = curr[1];
			curr += 2;

			//~ printf("TEST %i %u !\n", u, writ->free);

			if ( (l.tagPtr & 7) != (r.tagPtr & 7) )
				goto MISMATCH;

			if ( l.tagPtr & 7 ) {

				if (l.tag != r.tag)
					goto MISMATCH;

				switch(l.tag) {

					//~ case ValueTag_Non:
					//~ case ValueTag_Jst:
					//~ case ValueTag_Nil:
					//~ case ValueTag_Cns:
						//~ continue;

					//~ case ValueTag_Boo:
						//~ if ( l.Boo.val != r.Boo.val )
							//~ goto MISMATCH;
						//~ continue;

					case ValueTag_Int:
						//~ printf("Int\n");
						if ( l.Int.val != r.Int.val )
							goto MISMATCH;
						continue;

					//~ case ValueTag_Jst1u:
						//~ if ( l.Jst1u.car != r.Jst1u.car ) {
							//~ if ( writ->free < 2 )
								//~ CAllocator_push(&writ, 2);
							//~ writ->curr[0] = (Value) {value: l.Jst1u.car};
							//~ writ->curr[1] = (Value) {value: r.Jst1u.car};
							//~ writ->curr += 2;
						//~ }
						//~ continue;

					//~ case ValueTag_Cns1u:
						//~ if ( l.Cns1u.car != r.Cns1u.car ) {
							//~ if ( writ->free < 2 )
								//~ CAllocator_push(&writ, 2);
							//~ writ->curr[0] = (Value) {value: l.Cns1u.car};
							//~ writ->curr[1] = (Value) {value: r.Cns1u.car};
							//~ writ->curr += 2;
						//~ }
						//~ continue;

					default:
						printf("Function refl, unboxed case not handled : %s = %i\n", ValueTag_Names(l.tag), l.tag);
						exit(1);
						break;
				}
			}

			if ( l.value == r.value ) {
				//~ printf("EQU value %p %p \n", l.value, r.value);
				continue;
			}

			if (*l.ptag != *r.ptag)
				goto MISMATCH;

			switch(*l.ptag) {

				//~ case ValueTag_Str:
					//~ if ( l.Str->len != r.Str->len )
						//~ goto MISMATCH;
					//~ for(nat64 i = 0, j = l.Str->len; i < j; i++)
						//~ if (l.Str->str[i] != r.Str->str[i])
							//~ goto MISMATCH;
					//~ continue;

				//~ case ValueTag_Jst1b:
					//~ if ( writ->free < 2 )
						//~ CAllocator_push(&writ, 2);
					//~ writ->curr[0] = l.Jst1b->car;
					//~ writ->curr[1] = r.Jst1b->car;
					//~ writ->curr += 2;
					//~ continue;

				//~ case ValueTag_Cns1b:
					//~ if ( writ->free < 2 )
						//~ CAllocator_push(&writ, 2);
					//~ writ->curr[0] = l.Cns1b->car;
					//~ writ->curr[1] = r.Cns1b->car;
					//~ writ->curr += 2;
					//~ continue;

				//~ case ValueTag_Cns2:
					//~ if ( writ->free < 4 )
						//~ CAllocator_push(&writ, 4);
					//~ writ->curr[0] = l.Cns2->car;
					//~ writ->curr[1] = r.Cns2->car;
					//~ writ->curr[2] = l.Cns2->cdr;
					//~ writ->curr[3] = r.Cns2->cdr;
					//~ writ->curr += 4;
					//~ continue;

				//~ case ValueTag_Tup:
					//~ if ( l.Tup->len != r.Tup->len )
						//~ goto MISMATCH;
					//~ for(nat64 i = 0, j = l.Tup->len; i < j;) {

						//~ if ( writ->free < 2 * (j - i) )
							//~ CAllocator_push(&writ, 2 * (j - i));

						//~ for(nat64 k = min(writ->free / 2, j - i); k > 0; k--, i++) {
							//~ writ->curr[0] = l.Tup->arr[i];
							//~ writ->curr[1] = r.Tup->arr[i];
							//~ writ->curr += 2;
						//~ }

					//~ }
					//~ continue;

				case ValueTag_Arr:
					//~ printf("Arr\n");
					if ( l.Tup->len != r.Arr->len )
						goto MISMATCH;
					for(nat64 i = 0, j = l.Arr->len; i < j;) {

						if ( writ->free < 2 * (j - i) )
							if (CAllocator_push(&writ, 2 * (j - i)))
								head = writ;

						//~ printf("PROGRESS ARR %i %u !\n", u, writ->free);
						for(nat64 k = min(writ->free / 2, j - i); k > 0; k--, i++) {
							writ->curr[0] = l.Arr->arr[i];
							writ->curr[1] = r.Arr->arr[i];
							writ->curr += 2;
							writ->free -= 2;
						}

					}
					continue;


				default:
					printf("Function refl, boxed case not handled : %s = %i\n", ValueTag_Names(*l.ptag), *l.ptag);
					exit(1);
					break;

			}

		}

	v = (Value) {Boo: {tag: ValueTag_Boo, val: 1}};
	goto END;

	MISMATCH:
	v = (Value) {Boo: {tag: ValueTag_Boo, val: 0}};

	END:
	//~ if (u) printf("REFL STEPS : %i \n", u);
	CAllocator_free(head);
	return v;

}


//**************************************/
//
//**************************************/
bool Pattern_match(Value* sco, Value l, Value r) {

	CircularPage	head = &(S_CircularPage) {stck: 1, size: 128, free: 126, prev: NULL, next: NULL, curr: ((Value[128]) {l, r}) + 2};
	CircularPage	writ = head;
	CircularPage	read = head;
	Value*		curr = read->curr - 2;
	bool v;

	int u = 0;
	while (curr != writ->curr)
		if (curr == read->curr) {

			//~ printf("NEXT PAGE %p => %p !\n", read, read->next);
			read->curr -= read->size - read->free;
			read->free = read->size;
			read->prev = head;
			head->next = read;
			head = read;
			read = read->next;
			read->prev = NULL;
			head->next = NULL;

			//~ printf("NEXT PAGE %p !\n", read);
			curr = read->curr - read->size + read->free;
			//~ printf("NEXT PAGE %p read->curr : %p, curr : %p => %p, diff : %li !\n", read, read->curr, curr, curr + 2, read->curr - curr);

		} else while (curr != read->curr) {

			u++;
			//~ printf("PROGRESS %i %u !\n", u++, writ->free);
			l = curr[0];
			r = curr[1];
			curr += 2;

			//~ printf("TEST %i %u !\n", u, writ->free);

			if ( l.tagPtr != ValueTag_Var && (l.tagPtr & 7) != (r.tagPtr & 7) )
				goto MISMATCH;

			if ( l.tagPtr & 7 ) {

				if (l.tagPtr != ValueTag_Var && l.tag != r.tag)
					goto MISMATCH;

				switch(l.tag) {

					//~ case ValueTag_Non:
					//~ case ValueTag_Jst:
					//~ case ValueTag_Nil:
					//~ case ValueTag_Cns:
						//~ continue;

					//~ case ValueTag_Boo:
						//~ if ( l.Boo.val != r.Boo.val )
							//~ goto MISMATCH;
						//~ continue;

					case ValueTag_Int:
						//~ printf("Int\n");
						if ( l.Int.val != r.Int.val )
							goto MISMATCH;
						continue;

					case ValueTag_Var:
						//~ printf("Int\n");
						sco[l.Var.var - 1] = r;
						continue;

					//~ case ValueTag_Jst1u:
						//~ if ( l.Jst1u.car != r.Jst1u.car ) {
							//~ if ( writ->free < 2 )
								//~ CAllocator_push(&writ, 2);
							//~ writ->curr[0] = (Value) {value: l.Jst1u.car};
							//~ writ->curr[1] = (Value) {value: r.Jst1u.car};
							//~ writ->curr += 2;
						//~ }
						//~ continue;

					//~ case ValueTag_Cns1u:
						//~ if ( l.Cns1u.car != r.Cns1u.car ) {
							//~ if ( writ->free < 2 )
								//~ CAllocator_push(&writ, 2);
							//~ writ->curr[0] = (Value) {value: l.Cns1u.car};
							//~ writ->curr[1] = (Value) {value: r.Cns1u.car};
							//~ writ->curr += 2;
						//~ }
						//~ continue;

					default:
						printf("Function refl, unboxed case not handled : %s = %i\n", ValueTag_Names(l.tag), l.tag);
						exit(1);
						break;
				}
			}

			if ( l.value == r.value ) {
				//~ printf("EQU value %p %p \n", l.value, r.value);
				continue;
			}

			if (*l.ptag != *r.ptag)
				goto MISMATCH;

			switch(*l.ptag) {

				//~ case ValueTag_Str:
					//~ if ( l.Str->len != r.Str->len )
						//~ goto MISMATCH;
					//~ for(nat64 i = 0, j = l.Str->len; i < j; i++)
						//~ if (l.Str->str[i] != r.Str->str[i])
							//~ goto MISMATCH;
					//~ continue;

				//~ case ValueTag_Jst1b:
					//~ if ( writ->free < 2 )
						//~ CAllocator_push(&writ, 2);
					//~ writ->curr[0] = l.Jst1b->car;
					//~ writ->curr[1] = r.Jst1b->car;
					//~ writ->curr += 2;
					//~ continue;

				//~ case ValueTag_Cns1b:
					//~ if ( writ->free < 2 )
						//~ CAllocator_push(&writ, 2);
					//~ writ->curr[0] = l.Cns1b->car;
					//~ writ->curr[1] = r.Cns1b->car;
					//~ writ->curr += 2;
					//~ continue;

				//~ case ValueTag_Cns2:
					//~ if ( writ->free < 4 )
						//~ CAllocator_push(&writ, 4);
					//~ writ->curr[0] = l.Cns2->car;
					//~ writ->curr[1] = r.Cns2->car;
					//~ writ->curr[2] = l.Cns2->cdr;
					//~ writ->curr[3] = r.Cns2->cdr;
					//~ writ->curr += 4;
					//~ continue;

				//~ case ValueTag_Tup:
					//~ if ( l.Tup->len != r.Tup->len )
						//~ goto MISMATCH;
					//~ for(nat64 i = 0, j = l.Tup->len; i < j;) {

						//~ if ( writ->free < 2 * (j - i) )
							//~ CAllocator_push(&writ, 2 * (j - i));

						//~ for(nat64 k = min(writ->free / 2, j - i); k > 0; k--, i++) {
							//~ writ->curr[0] = l.Tup->arr[i];
							//~ writ->curr[1] = r.Tup->arr[i];
							//~ writ->curr += 2;
						//~ }

					//~ }
					//~ continue;

				case ValueTag_Arr:
					//~ printf("Arr\n");
					if ( l.Tup->len != r.Arr->len )
						goto MISMATCH;
					for(nat64 i = 0, j = l.Arr->len; i < j;) {

						if ( writ->free < 2 * (j - i) )
							if (CAllocator_push(&writ, 2 * (j - i)))
								head = writ;

						//~ printf("PROGRESS ARR %i %u !\n", u, writ->free);
						for(nat64 k = min(writ->free / 2, j - i); k > 0; k--, i++) {
							writ->curr[0] = l.Arr->arr[i];
							writ->curr[1] = r.Arr->arr[i];
							writ->curr += 2;
							writ->free -= 2;
						}

					}
					continue;


				default:
					printf("Function refl, boxed case not handled : %s = %i\n", ValueTag_Names(*l.ptag), *l.ptag);
					exit(1);
					break;

			}

		}

	v = true;
	goto END;

	MISMATCH:
	v = false;

	END:
	//~ printf("MATCH STEPS : %i with result : %i \n", u, v);
	CAllocator_free(head);
	return v;

}

