
//****************************************************************************/
// VStack
//****************************************************************************/

typedef struct Dump {
	XInstruction	C;
	Value_Arr	E;
	Value		A;
} Dump;

//**************************************/
// VStackData
//**************************************/
typedef union VStackData VStackData;
union VStackData {
	void**		data;
	Dump*		dump;
	Value_Kon	kont;
	Value*		value;
};

//**************************************/
// Stack
//**************************************/
typedef struct VStack* VStack;
typedef struct VStack S_VStack;
struct VStack {
	nat64		dim;
	nat64		size;
	nat64		free;
	VStack		prev;
	VStack		next;
	VStackData	data;
};



//~ #define PSHD(S)		{if (!S->free) XAllocator_push(&S); *(S->data.dump) = (Dump) {C: C, E:E, A:A}; S->data.dump++; S->free--;}
//~ #define POPD(S)		{if (S->data.data == (void**) (S + 1)) XAllocator_free(&S, 0); S->data.dump--; S->free++; C = S->data.dump->C; E = S->data.dump->E; A = S->data.dump->A;}
#define PSH(S, V)	{if (!S->free) XAllocator_push(&S, 1); *(S->data.value) = V; S->data.value++; S->free--;}
#define POP(S)		({if (S->data.data == (void**) (S + 1)) XAllocator_free(&S, 0); S->data.value--;  S->free++; *(S->data.value);})


//**************************************/
//
//**************************************/
VStack XAllocator_alloc(nat32 size, nat64 dim, VStack next) {
	VStack p = malloc(sizeof(S_VStack) + size * dim);
	*p = (S_VStack) {dim: dim, size: size, free: size, prev: NULL, next: next, data: {data: (void**) (p + 1)}};
	return p;
}

void XAllocator_push(VStack* stack, nat64 msize) {
	if ((*stack)->prev) {
		*stack = (*stack)->prev;
		return;
	}
	nat32 size = max(2 * (*stack)->size, msize);
	//~ printf("REALLOC at size : %i %lu\n", size, size * (*stack)->dim);
	VStack p = XAllocator_alloc(size, (*stack)->dim, *stack);
	(*stack)->prev = p;
	*stack = p;
}

void XAllocator_free(VStack* stack, int all) {
	if ((*stack)->prev) {
		free((*stack)->prev);
		(*stack)->prev = NULL;
	}
	if (!all)
		*stack = (*stack)->next;
	else free(*stack);
}

//**************************************/
//
//**************************************/

void copyValue(Value src, Value tgt) {
	switch(src.tag) {
		case ValueTag_Boo: return;
		case ValueTag_Int: return;
		case ValueTag_Nil: return;
		default: switch(src.value->tag) {
			//~ case ValueTag_Int:
				//~ tgt.value->type = src.value->type;
				//~ tgt.value->Int.n = src.value->Int.n;
			//~ break;
			//~ case ValueTag_Cns:
				//~ tgt.value->type = src.value->type;
				//~ tgt.value->Cns.head = src.value->Cns.head;
				//~ tgt.value->Cns.tail = src.value->Cns.tail;
			//~ break;
			case ValueTag_Clo:
				tgt.Clo->tag = src.Clo->tag;
				tgt.Clo->cod = src.Clo->cod;
				tgt.Clo->env = src.Clo->env;
			break;
			case ValueTag_Clj:
				tgt.Clj->tag = src.Clj->tag;
				tgt.Clj->cod = src.Clj->cod;
				tgt.Clj->env = src.Clj->env;
			break;
			//~ case ValueTag_Kon:
				//~ tgt.Kon->tag = src.Kon->tag;
				//~ tgt.Kon->cod = src.Kon->cod;
				//~ tgt.Kon->env = src.Kon->env;
				//~ tgt.Kon->arg = src.Kon->arg;
			//~ break;
			default:
				printf("copyValue dispatch error on value : %i \n", src.tag);
				exit(1);
		}
	}
}

#include "Refl.c"

Value UOPE_Car(Value a) {
	return a.Cns2->car;
}

Value UOPE_Cdr(Value a) {
	return a.Cns2->cdr;
}

Value BOPE_Cns(Value car, Value cdr) {
	S_Value_Cns2* v = malloc(sizeof(S_Value_Cns2));
	*v = (S_Value_Cns2){tag: ValueTag_Cns2, car: car, cdr: cdr};
	return ((Value) {Cns2: v});
}

Value BOPE_Acc(Value arr, Value idx) {
	return arr.Arr->arr[idx.Int.val];
}

Value BOPE_Equ(Value l, Value r) {
	return refl(l, r);
	return (Value) {Boo: {tag: ValueTag_Boo, val: l.Int.val == r.Int.val}};
}

Value BOPE_Grt(Value l, Value r) {
	return (Value) {Boo: {tag: ValueTag_Boo, val: l.Int.val > r.Int.val}};
}

Value BOPE_Sum(Value l, Value r) {
	return (Value) {Int: {tag: ValueTag_Int, val: l.Int.val + r.Int.val}};
}

Value BOPE_Mns(Value l, Value r) {
	return (Value) {Int: {tag: ValueTag_Int, val: l.Int.val - r.Int.val}};
}

Value BOPE_Prd(Value l, Value r) {
	return (Value) {Int: {tag: ValueTag_Int, val: l.Int.val * r.Int.val}};
}

Value apply(Value f, Value a) {
	if (f.tagPtr & 7) switch(f.tag) {
		case ValueTag_Cns:
			if (a.tagPtr & 7) {
				S_Value_Cns1b* v = malloc(sizeof(S_Value_Cns1b));
				*v = (S_Value_Cns1b){tag: ValueTag_Cns1b, car: a};
				return ((Value) {Cns1b: v});
			}
			return ((Value) {Cns1u: {tag: ValueTag_Cns1u, car: a.value}});
		default:
			printf("Function apply, unboxed case not handled : %s = %i\n", ValueTag_Names(f.tag), f.tag);
			exit(1);
			break;
	}
	switch(f.value->tag) {
		case ValueTag_Cns1b:
			S_Value_Cns2* v = malloc(sizeof(S_Value_Cns2));
			*v = (S_Value_Cns2){tag: ValueTag_Cns2, car: f.Cns1b->car, cdr: a};
			return ((Value) {Cns2: v});
		default:
			printf("Function apply, boxed case not handled : %s\n", ValueTag_Names(f.value->tag));
			exit(1);
			break;
	}
}

//**************************************/
//
//**************************************/
S_XInstruction Fix[] = {
	{cod: XOpCode_LABL},
	{cod: XOpCode_KNTN, dat: {cljr: {addr: 1, size: 0}}},
	{cod: XOpCode_APLY},
	{cod: XOpCode_VRBL, dat: {vrbl: 0}},
	{cod: XOpCode_COPY},
	{cod: XOpCode_CNTN},
};

//**************************************/
//
//**************************************/

Value SECD_run(VStack S, VStack K, Value A, XInstruction C, Value* E, VStack L) {

	Value l, r, f, x;
	Value_Kon k;



	for (nat64 step = 0; DEBUG(printf("PROCESS STEP %05lu : OpCode(%s) @ %p  => \n", step, XOpCode_Names[C->cod], C)); step++, C++)

		switch(C->cod) {

			case XOpCode_NOPE:
				break;

			case XOpCode_DONE:
				printf("STEPS : %lu\n", step);
				return A;
				break;

			//**************************************/
			// JUMP
			//**************************************/
			case XOpCode_LABL:
				break;

			case XOpCode_JUMP:
				C += C->dat.addr;
				break;

			case XOpCode_JMPT:
				if ( POP(S).Boo.val ) C += C->dat.addr;
				break;

			case XOpCode_JMPF:
				if ( !POP(S).Boo.val ) C += C->dat.addr;
				break;

			//**************************************/
			// Vars
			//**************************************/
			case XOpCode_VARa:
				PSH(S, A);
				break;

			case XOpCode_VARe:
				PSH(S, E[C->dat.vrbl]);
				break;

			case XOpCode_VARl:
				PSH(S, L->data.value[-C->dat.vrbl]);
				break;

			//**************************************/
			// Lambda Calculus
			//**************************************/
			case XOpCode_CLJR: {
				S_Value_Clo* clo = malloc(sizeof(S_Value_Clo) + sizeof(Value) * C->dat.cljr.size);
				*clo = (S_Value_Clo) {
					tag: ValueTag_Clo,
					cod: C + C->dat.cljr.addr,
					len: C->dat.cljr.size,
					env: (Value*) (clo + 1)
				};
				for(nat32 i = 0, j = C->dat.cljr.size, v; i < j; i++) {
					v = (C++ + 1)->dat.vrbl;
					clo->env[i] = !v ? A : E[v - 1];
				}
				PSH(S, ((Value){Clo: clo}));
				break;
			}

			case XOpCode_KNTN: {
				nat64 size = sizeof(S_Value_Kon) + sizeof(Value) * C->dat.cljr.size + sizeof(void*);

				if (size > K->free) {
					//~ printf("OK AAAAA %lu %lu %lu \n", K->size, K->free, size);
					XAllocator_push(&K, size);
					//~ printf("OK BBBBB %lu %lu %lu \n", K->size, K->free, size);
				}

				//~ printf("OK 11111 \n");

				S_Value_Kon* kntn = K->data.kont;
				K->data.data += size / 8;
				K->free -= size;

				//~ printf("OK 22222 %lu %lu %p \n", K->size, K->free, K);
				*kntn = (S_Value_Kon) {
					tag: ValueTag_Kon,
					cod: C + C->dat.cljr.addr,
					len: C->dat.cljr.size,
					env: (Value*) (kntn + 1)
				};
				//~ printf("OK 33333 \n");
				for(nat32 i = 0, j = C->dat.cljr.size, v; i < j; i++) {
					//~ printf("OK %u / %u\n", i, j);
					v = (C++ + 1)->dat.vrbl;
					kntn->env[i] = !v ? A : E[v - 1];
				}
				Value_Kon* k = (Value_Kon*) (K->data.data - 1);
				*k = kntn;
				break;
			}

			case XOpCode_CNTN:
				//~ LABL_CNTN:
				if ( K->data.data == (void*) (K + 1) ) {
					//~ printf("We free\n");
					XAllocator_free(&K, 0);
				}
				A = POP(S);
				k = (Value_Kon) *(K->data.data - 1);
				C = k->cod;
				E = k->env;
				K->free += (K->data.data - (void**) k) * 8;
				K->data.kont = k;
				break;

			case XOpCode_APLY:
				x = POP(S);
				f = POP(S);
				if ( !(f.tagPtr & 7) && f.Clo->tag == ValueTag_Clo ) {
					C = f.Clo->cod;
					E = f.Clo->env;
					A = x;
				} else {
					//~ PSH(S, K);
					//~ PSH(S, apply(f, x));
					printf("EXIT in APLY !!\n");
					exit(1);
					//~ goto LABL_CNTN;
				}
				break;

			//**************************************/
			// FixPoint Operator
			//**************************************/
			case XOpCode_FIXP: {
				f = POP(S);
				x = (Value) {value: malloc(sizeof(S_Value))};
				PSH(S, x);
				PSH(S, f);
				PSH(S, x);
				C = Fix;
				break;
			}

			//**************************************/
			//
			//**************************************/
			case XOpCode_COPY:
				f = POP(S);
				x = POP(S);
				copyValue(f, x);
				PSH(S, x);
				break;

			//**************************************/
			//
			//**************************************/
			case XOpCode_LOAD:
				PSH(S, *C->dat.valu);
				break;

			//~ case XOpCode_UOPE:
				//~ PSH(S, C->dat.uope(POP(S)));
				//~ break;

			case XOpCode_BOPE:
				r = POP(S);
				l = POP(S);
				PSH(S, C->dat.bope(l, r));
				break;

			//**************************************/
			// Array
			//**************************************/
			case XOpCode_ARAY: {
				Value_Arr arr = malloc(sizeof(S_Value_Arr) + C->dat.size * sizeof(Value));
				*arr = (S_Value_Arr){tag: ValueTag_Arr, len: C->dat.size, arr: (Value*) (arr + 1)};
				for(nat64 i = C->dat.size; i > 0; i--)
					arr->arr[i - 1] = POP(S);
				PSH(S, (Value){Arr: arr});
				break;
			}


			//**************************************/
			// Match
			//**************************************/
			case XOpCode_MTCH: {
				Value* sco = malloc(sizeof(Value) * C->dat.size);
				Value m = POP(S);
				nat32 v;
				for(; (C + 1)->cod == XOpCode_BRCH; C++) {
					C++;
					if (!Pattern_match(sco, *C->dat.patt, m)) {
						C += (C + 1)->dat.addr;
						continue;
					}
					C += 2;
					for(int i = 0; C->cod == XOpCode_CPTR; i++, C++) {
						v = C->dat.vrbl;
						sco[i] = !v ? A : E[v - 1];
					}
					if (C->cod == XOpCode_GARD) {
						x = SECD_run(S, K, A, C + C->dat.addr, sco, L);
						if (!x.Boo.val) {
							C++;
							continue;
						}
					} else C--;
					E = sco;
					break;
				}
				break;
			}

			//*****************************************/
			// Deprecated
			//*****************************************/
			case XOpCode_RTRN:
				printf("STEPS : %lu\n", step);
				return A;
				break;

			case XOpCode_VRBL:
				if (!C->dat.vrbl) {
					PSH(S, A);
				} else {
					PSH(S, E[C->dat.vrbl - 1]);
				}
				break;

			//*****************************************/
			//*****************************************/
			default:
				printf("MISSING implementation for XOpCode(%s) !\n", XOpCode_Names[C->cod]);
				exit(1);
		}

	return (Value){Int: {tag: ValueTag_Int, val: -1}};

}


//**************************************/
//
//**************************************/
Value SECD_start(XInstruction C, Value* E) {

	VStack	L	= XAllocator_alloc(128, sizeof(Value), NULL);
	VStack	S	= XAllocator_alloc(128, sizeof(Value), NULL);
	VStack	K 	= XAllocator_alloc(128, sizeof(char), NULL);
	Value	A	= {tag: ValueTag_Nil};

	A = SECD_run(S, K, A, C, E, L);

	XAllocator_free(&L, 1);
	XAllocator_free(&S, 1);
	XAllocator_free(&K, 1);

	return A;

}
