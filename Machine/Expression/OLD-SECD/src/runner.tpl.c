
#define	CLJR(lbl, A, E)		makeValue_Clo(lbl, mkEnv(A, E))
#define PSHK(lbl, A, E)		initValue_Kon(K->current, lbl, E, A); K->current++
#define CALL(f, a)		E = (f).Clo->env; C = (f).Clo->cod; A = a; goto *C
#define RTRN(x)			Z = x; K->current--; CALL(((Value) K->current), K->current->arg)

#define PSHN(X)			S->current += X
#define PSH(X)			*(S->current) = X; S->current++
#define POP(N)			S->current -= N

Value runner(Values E) {
	
	printf("New Runner instance\n");

	StackFrame	S	= allocStack(NULL);
	KontFrame	K 	= allocKont(NULL);
	Value		A	= (Value) (S_Value*) 42;
	Value		Z	= (Value) (S_Value*) 43;
	void*		C;
	
	int step = 0;
	
	int getStack() {
		return S->current - ((Value*) (S + 1));
	}
	
	int getKonts() {
		return K->current - ((S_Value_Kon*) (K + 1));
	}
	
	void printState(char* name) {
		step++;
		printf("STEP %4i: %8s \t\t(%3i, %2i) (A= %3s, Z= %3s)\n", step, name, getStack(), getKonts(), nameValue(A), nameValue(Z));
	}
	

	PSHK(&&RETURN, A, E);
	
	Value {L.implode ', ' rxs};
	
	goto MAIN;
	
	RETURN:
		return Z;

	FIX:	rx0 = makeValue();
		PSHK(&&FIZ, rx0, E);
		CALL(A, rx0);
	FIZ:	copyValue(Z, A);
		RTRN(Z);
			
	PROGRAM:
		{indent code}
	
}
