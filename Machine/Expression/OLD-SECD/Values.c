

Value makeValue() {
	return (Value) {value: malloc(sizeof(S_Value))};
}



Value initValue_Clo(Value_Clo v, Instruction i, Values e) {
	v->tag = ValueTag_Clo;
	v->cod = i;
	v->env = e;
	return (Value) v;
}

Value makeValue_Clo(Instruction i, Values e) {
	return initValue_Clo(malloc(sizeof(S_Value_Clo)), i, e);
}

Value initValue_Kon(Value_Kon v, Instruction i, Values e, Value z) {
	v->tag = ValueTag_Kon;
	v->cod = i;
	v->env = e;
	v->arg = z;
	return (Value) v;
}

Value makeValue_Kon(Instruction i, Values e, Value z) {
	return initValue_Kon(malloc(sizeof(S_Value_Kon)), i, e, z);
}

void copyValue(Value src, Value tgt) {
	switch(src.tag) {
		case ValueTag_Boo: return;
		case ValueTag_Nat: return;
		//~ case ValueTag_Nil: return;
	}
	
	switch(src.value->tag) {
		//~ case ValueTag_Int: 
			//~ tgt.value->type = src.value->type;
			//~ tgt.value->Int.n = src.value->Int.n;
		//~ break;
		//~ case ValueTag_Cns: 
			//~ tgt.value->type = src.value->type;
			//~ tgt.value->Cns.head = src.value->Cns.head;
			//~ tgt.value->Cns.tail = src.value->Cns.tail;
		//~ break;
		case ValueTag_Clo: 
			tgt.Clo->tag = src.Clo->tag;
			tgt.Clo->cod = src.Clo->cod;
			tgt.Clo->env = src.Clo->env;
		break;
		case ValueTag_Kon: 
			tgt.Kon->tag = src.Kon->tag;
			tgt.Kon->cod = src.Kon->cod;
			tgt.Kon->env = src.Kon->env;
			tgt.Kon->arg = src.Kon->arg;
		break;
		default:
			printf("copyValue dispatch error on value : %i \n", src.tag);
			exit(1);
	}
}











Value BOP_PLUS(Value l, Value r) {
	switch((l.tag << 16) + r.tag) {
		case (ValueTag_Nat << 16) + ValueTag_Nat:
			return (Value) {Nat: {tag: ValueTag_Nat, nat: l.Nat.nat + r.Nat.nat}};
		default:
			printf("BOP PLUS missing ok %i , %i\n", l.tag, r.tag);
			exit(1);
	}
}


Value BOP_MINUS(Value l, Value r) {
	switch((l.tag << 16) + r.tag) {
		case (ValueTag_Nat << 16) + ValueTag_Nat:
			return (Value) {Nat: {tag: ValueTag_Nat, nat: l.Nat.nat - r.Nat.nat}};
		default:
			printf("BOP MINUS missing\n");
			exit(1);
	}
}

Boo BOP_LTE_(Value l, Value r) {
	switch((l.tag << 16) + r.tag) {
		case (ValueTag_Nat << 16) + ValueTag_Nat:
			return l.Nat.nat <= r.Nat.nat;
		default:
			printf("BOP LTE missing\n");
			exit(1);
	}
}
 

Value BOP_LTE(Value l, Value r) {
	return (Value) (S_Value_Boo) {tag: ValueTag_Boo, boo: BOP_LTE_(l, r)};
}



