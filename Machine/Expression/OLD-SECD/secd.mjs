import program from "./auto/bytecode.json" assert {type: "json"}
import util from "util"

const log = o => console.log(util.inspect(o, {showHidden: false, depth: null, colors: true}))



function dep(n) {
	var x = 0;
	while(n--)
		x++;
	return x;
}

function fib($n) {
	if ($n <= 0)
		return 1;
	return fib($n - 1) + fib($n - 1);
}

var n = 30;
var m = 1000_0000000;
console.log(`fib ${n} = `, fib(n));
//~ console.log(`dep ${m} = `, dep(m));

function Cns(head, tail) {
	return [head, tail]
}

var tot = 0
function run(S, E, C, c = -1) {
	var i, show = false
	
//~ APP,		x :> Clo F (Lam v b) :> st	-> run F[(v):= x] b $ is :> E :> st
//~ APP,		x :> Fix f :> st		-> run E (APP :> SWAP :> APP :> is) $ Fix f :> f :> x :> st
	
	function apply(f, a) {
		//~ tot++
		switch(f.type) {
			case "CLO":
				S.push([E, C, c])
				E = Object.assign({}, f.E, {[f.v]: a})
				C = f.b
				c = 0
				break
			//~ case "Fix":
				//~ S.push([C, c])
				//~ C = [{type: "APP"}, {type: "SWAP"}, {type: "APP"}, {type: "POP"}]
				//~ c = 0
				//~ S.push(a)
				//~ S.push(f.f)
				//~ S.push(f)
				//~ break;
			default:
				console.log("No case for apply", {f, a, S, E, C, c})
				process.exit()
		}
	}
	
	function fix(f) {
		var nE = Object.assign({}, f.E, {[f.v]: {}})
		var r = run([], nE, f.b, 0, 1)
		Object.assign(nE[f.v], r.S[0])
		return r.S[0];
	}
	
	
	while (C[++c]) {
		//~ console.log("\n\n\n")
		//~ if ( show || 1) console.log(util.inspect({c, S, E, C}, {showHidden: false, depth: null, colors: true}))
		i = C[c]
		switch (i.type) {
			
			case "JMP":
				c = i.l
				break
			
			//~ case "VAR":
				//~ S.push(E[i.v])
				//~ break
			
			//~ case "BOO":
				//~ S.push(i.b)
				//~ break
			
			//~ case "INT":
				//~ S.push(i.n)
				//~ break
			
			//~ case "STR":
				//~ S.push(i.s)
				//~ break
				
			case "LAM":
				S.push({type: "CLO", E: E, b: i.b})
				break
				
			case "FIX":
				var f = S.pop()
				var x = {}
				S.push(x);
				S.push(f);
				S.push(x);
				break;
			
			case "FIZ":
				var fixf = S.pop()
				var x = S.pop()
				copyValue(fixf, x);
				PUSH(S) = fixf;
				break;
			
//~ FIX,		s :> st				-> run E (APP :> is) $ (Fix s) :> s :> st
			//~ case "FIX":
				//~ var f = S.pop()
				//~ S.push(fix(f))
				//~ break
				
			//~ case "RET":
				//~ if (inner)
					//~ break;
				//~ var v = S.pop()
				//~ var x = S.pop()
				//~ E = x[0]
				//~ C = x[1]
				//~ c = x[2]
				//~ S.push(v)
				//~ break
			
			//~ case "POP":
				//~ var x = S.pop()
				//~ var y = S.pop()
				//~ S.push(x)
				//~ C = y [0]
				//~ c = y[1]
				//~ console.log({x, y})
				//~ process.exit()
				//~ break;
			//~ case "Alt":
				//~ var x = S.pop()
				//~ C.push(...(x ? i.l : i.r))
				//~ S.push([C, c])
				//~ C = x ? i.l : i.r
				//~ c = 0
				//~ continue
				
			//~ case "APP":
				//~ var a = S.pop()
				//~ var f = S.pop()
				//~ apply(f, a)
				//~ continue
				//~ break
			
			//~ case "MOD":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l % r)
				//~ break
			
			//~ case "DIV":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l / r)
				//~ break
			
			//~ case "ADD":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l + r)
				//~ break
			
			//~ case "MIN":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l - r)
				//~ break
			
			//~ case "MUL":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l * r)
				//~ break
		
			
			//~ case "EQI":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l == r)
				//~ break
				
			//~ case "LTE":
				//~ var r = S.pop()
				//~ var l = S.pop()
				//~ S.push(l <= r)
				//~ break
				
			//~ case "SWAP":
				//~ var x = S.pop()
				//~ var y = S.pop()
				//~ S.push(x)
				//~ S.push(y)
				//~ break
				
			default:
				console.log("No case for run", {i, c, S, E, C})
				process.exit()
		}
	}
	
	//~ console.log({S, E, C})
	return  {c, S, E, C}
}	
//~ console.log(run([], {}, program))
//~ log(run([], {}, program, -1), {showHidden: false, depth: null, colors: true})
//~ log({tot})