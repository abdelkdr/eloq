#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>


#define N ((Nat64) 30)
//~ #define M ((Nat64) 1)
#define M ((Nat64) 50000000)


typedef		bool				Boo;
typedef		int64_t				Int64;
typedef		uint64_t			Nat64;

typedef		struct S_Values*		Values;
typedef		union S_ValueMark {
		Nat64				i;
} ValueMark;


#include "auto/header.h"


typedef struct S_Values {
	Value		head;
	Values		tail;
} S_Values;







Nat64 last = 0;
Nat64 frees = 0;
Values envs;

int len(Values v) {
	if ( !v )
		return 0;
	return 1 + len(v->tail);
}

Values addEnv(Values tail) {
	if (!frees) {
		last = (last * 2 + 1);
		envs = (Values) malloc(last * sizeof(S_Values));
		frees = last;
		//~ printf("ok %i\n", ii++);
	}
	//~ Values vs = malloc(sizeof(S_Values));
	Values vs = envs;
	envs++;
	frees--;
	vs->tail = tail;
	return vs;
}

Values mkEnv(Value v, Values vs) {
	Values nvs = addEnv(vs);
	nvs->head = v;
	return nvs;
}

Value getValues(Values vs, int v) {
	if (!v)
		return vs->head;
	return getValues(vs->tail, v - 1);
}

//~ #include "runtime/Machine.c"
//~ #include "auto/bytecode.c"
#include "Values.c"
#include "auto/runner.c"



//~ Value pw(Value n) {
	//~ if (BOP_LTE_(n, (Value)(S_Value_Nat){tag: ValueTag_Nat, nat: 1}))
		//~ return (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1};
	//~ return 
		//~ BOP_PLUS(
			//~ pw(BOP_MINUS(n, (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1})),
			//~ pw(BOP_MINUS(n, (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1}))
		//~ );
		
//~ }




Value pw(Value n) {
	if (BOP_LTE(n, (Value)(S_Value_Nat){tag: ValueTag_Nat, nat: 0}).Boo.boo)
		return (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1};
	return 
		BOP_PLUS(
			pw(BOP_MINUS(n, (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1})),
			pw(BOP_MINUS(n, (Value) (S_Value_Nat) {tag: ValueTag_Nat, nat: 1}))
		);
		
}




int main(int argc, char** argv) {
	Value res;
	Nat64 i = 128;
	Nat64 j = 64;
	printf("div: %lu\n", i / j);
	return 0;
	//~ res = (Value) {Nat: {tag: ValueTag_Nat, nat: loop(M)}};
	//~ res = (Value) {Nat: {tag: ValueTag_Nat, nat: ifib(N)}};
	res = pw((Value) {Nat: {tag: ValueTag_Nat, nat: N}});
	//~ res = machine(program_code, NULL);
	//~ res = runner(NULL);
	printf("fib %lu = %lu\n", N, res.Nat.nat);
	return 0;
}

