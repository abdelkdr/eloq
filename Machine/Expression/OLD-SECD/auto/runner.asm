
root:
	JMP main

clo_3:
	(VAR "reg0" 0)
	(SET "reg1" (Int 0))
	(BOPF "<=" "reg2" "reg0" "reg1")
	(JMPT "reg2" "ife_0")
	(VAR "reg3" 1)
	(VAR "reg4" 0)
	(SET "reg5" (Int 1))
	(BOPF "-" "reg6" "reg4" "reg5")
	(PUSHK "app_1")
	(CALL "reg3" "reg6")

app_1:
	(VAR "reg0" 1)
	(VAR "reg1" 0)
	(SET "reg2" (Int 1))
	(BOPF "-" "reg3" "reg1" "reg2")
	(PUSHK "app_2")
	(CALL "reg0" "reg3")

app_2:
	(POP "reg0")
	(POP "reg1")
	(BOPF "+" "reg2" "reg1" "reg0")
	(POPK "k")
	(PUSH "reg2")
	(KALL "k")

ife_0:
	(SET "reg0" (Int 1))
	(POPK "k")
	(PUSH "reg0")
	(KALL "k")

clo_4:
	(CLO "reg0" "clo_3")
	(POPK "k")
	(PUSH "reg0")
	(KALL "k")

main:
	(CLO "reg0" "clo_4")
	(SET "reg1" Val)
	(PUSHK "FIX_f_5")
	(PUSH "reg1")
	(CALL "reg0" "reg1")

FIX_f_5:
	(POP "reg0")
	(POP "reg1")
	(CPY "reg0" "reg1")
	(SET "reg2" (Int "N"))
	(PUSHK "app_6")
	(CALL "reg0" "reg2")

app_6:
	(POP "reg0")
	(END "reg0")