
#define SFRAME(Nam, Typ) typedef struct S_##Nam* Nam; \
typedef struct S_##Nam {\
	int		size;\
	Nam		left;\
	Nam		right;\
	Typ		current;\
} S_##Nam

SFRAME(Frame, void*);
SFRAME(StackFrame, Value*);
SFRAME(KontFrame, S_Value_Kon*);

void* alloc_Frame(Frame f, size_t s) {
	if ( f && f->left )
		return f->left;
	int size = f ? f->size * 2 : 100;
	printf("Alloc with size %i * %lu + struct = %lu\n", size, s, size * s + sizeof(S_Frame));
	Frame nf = malloc(sizeof(S_Frame) + size * s);
	nf->size = size;
	nf->left = NULL;
	nf->right = f;
	nf->current = (void*) (nf + 1);
	if (f) f->left = nf;
	return nf;
}




#define NCHECKIN(N, U)		if ((U->current + N) > (((typeof (U->current)) (U + 1)) + U->size)) U = alloc_Frame((Frame) U, sizeof(*(U->current))); U->current += N
#define NCHECKOUT(N, U)		if ((U->current - N) < ((typeof (U->current)) (U + 1))) U = U->right
#define NPSHK(N, lbl, A, E)	initValue_Kon((K->current N), lbl, E, A)
#define NRTRN(N, x)		Z = x; NCHECKOUT(N, K); K->current -= N; CALL(((Value) K->current), K->current->arg)


#define CHECKIN(U)		if (U->current == (((typeof (U->current)) (U + 1)) + U->size)) U = alloc_Frame((Frame) U, sizeof(*(U->current)))
#define CHECKOUT(U)		if (U->current == ((typeof (U->current)) (U + 1))) U = U->right


#define	CLJR(lbl, A, E)		makeValue_Clo(lbl, mkEnv(A, E))
#define PSHK(lbl, A, E)		initValue_Kon(K->current, lbl, E, A); K->current++; CHECKIN(K)
#define CALL(f, a)		E = (f).Clo->env; C = (f).Clo->cod; A = a; goto *C
#define oRTRN(x)		Z = x; K->current--; CALL(((Value) K->current), K->current->arg)
#define RTRN(x)			NRTRN(1, x)

#define PSHN(X)			S->current += X
#define PSH(X)			*(S->current) = X; S->current++; CHECKIN(Value*, S);
#define PSHS(X, N)		S->current[N] = X;
#define POP(N)			S->current -= N

Value runner(Values E) {

	StackFrame	S	= alloc_Frame(NULL, sizeof(*S->current));
	KontFrame	K 	= alloc_Frame(NULL, sizeof(*K->current));
	Value		A	= (Value) (S_Value*) 42;
	Value		Z	= (Value) (S_Value*) 43;
	void*		C;
	int fuel = 100;

	printf("New Runner instance : %lu\n", sizeof((*(K->current))));

	int step = 0;

	int getStack() {
		return S->current - ((Value*) (S + 1));
	}

	int getKonts() {
		return K->current - ((S_Value_Kon*) (K + 1));
	}

	void printState(char* name) {
		return;
		char buf[4];
		if (step++ && ! (step % 100) ) {
			read(0, buf, 1);
		}
		printf("STEP %4i: %8s \t\t(%3i, %2i) (A= %3s, Z= %3s)\n", step, name, getStack(), getKonts(), nameValue(A), nameValue(Z));
	}


	PSHK(&&RETURN, A, E);

	Value rx0, rx1, rx2, rx3, rx4, rx5, rx6, rx7, rx8, rx9, rx10, rx11, rx12, rx13, rx14;

	goto MAIN;

	RETURN:
		return Z;

	FIX:	rx0 = makeValue();
		PSHK(&&FIZ, rx0, E);
		CALL(A, rx0);
	FIZ:	copyValue(Z, A);
		RTRN(Z);

	PROGRAM:

		KON_12: //printState("KON_12");

			rx0 = *(S->current - 1);
			rx1 = BOP_PLUS(rx0, Z);
			POP(1);
			//~ exit(1);
			NRTRN(1, rx1);
			//~ oRTRN(rx1);
		//\f -> \x -> x <= 0 ? 1 : f (x - 1) \z_2 => f (x - 1) \z_3 => z_2 + z_3
		KON_13: //printState("KON_13");
			PSHS(Z, -1);
			rx2 = E->head;
			rx3 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
			rx4 = BOP_MINUS(A, rx3);
			//~ PSHK(&&KON_12, A, E);
			//~ K->current-= 2;
			CALL(rx2, rx4);
fun =
		LAM_14: //printState("LAM_14");

			rx5 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 0}});
			if (BOP_LTE_(A, rx5)) {
				//~ PSHN(0);
				rx7 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
				RTRN(rx7);
			} else {
				PSHN(1);
				rx8 = E->head;
				rx9 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
				rx10 = BOP_MINUS(A, rx9);
				NCHECKIN(2, K);
				NPSHK(-2, &&KON_12, A, E);
				NPSHK(-1, &&KON_13, A, E);
				//~ PSHK(&&KON_13, A, E);
				CALL(rx8, rx10);
			}

		LAM_16: //printState("LAM_16");

			rx11 = CLJR(&&LAM_14, A, E);
			RTRN(rx11);

		KON_19: //printState("KON_19");

			//~ POP(0);
			RTRN(Z);

		KON_20: //printState("KON_20");

			rx12 = ((Value) {Nat: {tag: ValueTag_Nat, nat: N}});
			PSHK(&&KON_19, A, E);
			CALL(Z, rx12);

		MAIN: //printState("MAIN");

			rx13 = CLJR(&&FIX, A, NULL);
			rx14 = CLJR(&&LAM_16, A, E);
			PSHK(&&KON_20, A, E);
			CALL(rx13, rx14);

}



