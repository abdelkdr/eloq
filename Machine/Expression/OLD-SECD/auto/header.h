typedef enum ValueTag {
	ValueTag_Array                 = 8,
	ValueTag_Clo                   = 16,
	ValueTag_Kon                   = 24,
	ValueTag_Boo                   = 1,
	ValueTag_Nat                   = 2,
} ValueTag;
typedef enum OpCodeTag {
	OpCodeTag_APP,
	OpCodeTag_END,
	OpCodeTag_RET,
	OpCodeTag_FIX,
	OpCodeTag_CPY,
	OpCodeTag_UOP,
	OpCodeTag_BOP,
	OpCodeTag_CST,
	OpCodeTag_VAR,
	OpCodeTag_CLO,
	OpCodeTag_LBL,
	OpCodeTag_JMP,
	OpCodeTag_ALT,
} OpCodeTag;
typedef union U_Value Value;
typedef union S_Value S_Value;
typedef Value (*Value_UnaryOp)(Value);
typedef Value (*Value_BinaryOp)(Value, Value);
typedef struct S_Value_Array S_Value_Array;
typedef struct S_Value_Clo S_Value_Clo;
typedef struct S_Value_Kon S_Value_Kon;
typedef struct S_Value_Array* Value_Array;
typedef struct S_Value_Clo* Value_Clo;
typedef struct S_Value_Kon* Value_Kon;
typedef struct S_Value_Boo {
	ValueTag                       tag;
	Boo                            boo;
} S_Value_Boo;
typedef struct S_Value_Nat {
	ValueTag                       tag;
	Nat64                          nat;
} S_Value_Nat;
typedef union U_Value {
	Boo                            boo;
	Nat64                          nat;
	ValueTag                       tag;
	ValueTag*                      ptag;
	S_Value*                       value;
	S_Value_Boo                    Boo;
	S_Value_Nat                    Nat;
	S_Value_Array*                 Array;
	S_Value_Clo*                   Clo;
	S_Value_Kon*                   Kon;
} U_Value;
typedef union S_Instruction* Instruction;
typedef union S_Instruction S_Instruction;
typedef struct S_Instruction_APP {
	OpCodeTag                      tag;
} S_Instruction_APP;
typedef struct S_Instruction_END {
	OpCodeTag                      tag;
} S_Instruction_END;
typedef struct S_Instruction_RET {
	OpCodeTag                      tag;
} S_Instruction_RET;
typedef struct S_Instruction_FIX {
	OpCodeTag                      tag;
} S_Instruction_FIX;
typedef struct S_Instruction_CPY {
	OpCodeTag                      tag;
} S_Instruction_CPY;
typedef struct S_Instruction_UOP {
	OpCodeTag                      tag;
	Value_UnaryOp                  uop;
} S_Instruction_UOP;
typedef struct S_Instruction_BOP {
	OpCodeTag                      tag;
	Value_BinaryOp                 bop;
} S_Instruction_BOP;
typedef struct S_Instruction_CST {
	OpCodeTag                      tag;
	Value                          val;
} S_Instruction_CST;
typedef struct S_Instruction_VAR {
	OpCodeTag                      tag;
	Nat64                          var;
} S_Instruction_VAR;
typedef struct S_Instruction_CLO {
	OpCodeTag                      tag;
	Instruction                    lbl;
} S_Instruction_CLO;
typedef struct S_Instruction_LBL {
	OpCodeTag                      tag;
	Instruction                    lbl;
} S_Instruction_LBL;
typedef struct S_Instruction_JMP {
	OpCodeTag                      tag;
	Instruction                    lbl;
} S_Instruction_JMP;
typedef struct S_Instruction_ALT {
	OpCodeTag                      tag;
	Instruction                    lbl;
	Instruction                    lbr;
} S_Instruction_ALT;
typedef union S_Instruction {
	OpCodeTag                      tag;
	S_Instruction_APP              APP;
	S_Instruction_END              END;
	S_Instruction_RET              RET;
	S_Instruction_FIX              FIX;
	S_Instruction_CPY              CPY;
	S_Instruction_UOP              UOP;
	S_Instruction_BOP              BOP;
	S_Instruction_CST              CST;
	S_Instruction_VAR              VAR;
	S_Instruction_CLO              CLO;
	S_Instruction_LBL              LBL;
	S_Instruction_JMP              JMP;
	S_Instruction_ALT              ALT;
} S_Instruction;
typedef struct S_Value_Array {
	ValueTag                       tag;
	ValueMark                      mark;
	Nat64                          len;
	Value*                         arr;
} S_Value_Array;
typedef struct S_Value_Clo {
	ValueTag                       tag;
	ValueMark                      mark;
	Instruction                    cod;
	Values                         env;
} S_Value_Clo;
typedef struct S_Value_Kon {
	ValueTag                       tag;
	ValueMark                      mark;
	Instruction                    cod;
	Values                         env;
	Value                          arg;
} S_Value_Kon;
typedef union S_Value {
	ValueTag                       tag;
	S_Value_Array                  Array;
	S_Value_Clo                    Clo;
	S_Value_Kon                    Kon;
} S_Value;
char* nameValue_(Value v) { switch(*(v.ptag)) { 
	case ValueTag_Array            : return "Array";
	case ValueTag_Clo              : return "Clo";
	case ValueTag_Kon              : return "Kon";
 } };
char* nameInstruction(Instruction i) { switch(i->tag) { 
	case OpCodeTag_APP             : return "APP";
	case OpCodeTag_END             : return "END";
	case OpCodeTag_RET             : return "RET";
	case OpCodeTag_FIX             : return "FIX";
	case OpCodeTag_CPY             : return "CPY";
	case OpCodeTag_UOP             : return "UOP";
	case OpCodeTag_BOP             : return "BOP";
	case OpCodeTag_CST             : return "CST";
	case OpCodeTag_VAR             : return "VAR";
	case OpCodeTag_CLO             : return "CLO";
	case OpCodeTag_LBL             : return "LBL";
	case OpCodeTag_JMP             : return "JMP";
	case OpCodeTag_ALT             : return "ALT";
 } };
char* nameValue(Value v) { switch(v.tag) { 
	case ValueTag_Boo              : return "Boo";
	case ValueTag_Nat              : return "Nat";
 	default: return nameValue_(v);
} };