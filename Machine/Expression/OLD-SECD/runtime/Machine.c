
S_Instruction micro[] = {
	{LBL: {tag: OpCodeTag_LBL}},
	{APP: {tag: OpCodeTag_APP}},
	{CPY: {tag: OpCodeTag_CPY}},
	{RET: {tag: OpCodeTag_RET}},
};

Value machine(Instruction C, Values E) {
	printf("New Machine instance \n");
	
	uint64_t step	= 0;
	Values	S	= NULL;
	Dump	D 	= NULL;
	Value	V;
	
	Value f, a, l, r, b, fx, x, z;
	while(true) {
		
		C++;
		step++;
		//~ printf("position: %p => %s :: %i, %i \n", C, nameInstruction(C), len(S), lend(D));
		switch(C->tag) {
			
			case OpCodeTag_VAR:
				if (C->VAR.var == 0) {
					PUSH(S) = V;
				} else {
					PUSH(S) = getValues(E, C->VAR.var - 1);
				}
				break;
			
			case OpCodeTag_CPY:
				fx = POP(S);
				x = POP(S);
				copyValue(fx, x);
				PUSH(S) = fx;
				break;
			
			case OpCodeTag_JMP:
				C = C->JMP.lbl;
				break;
			
			case OpCodeTag_ALT:
				b = POP(S);
				C = b.Boo.boo ? C->ALT.lbl : C->ALT.lbr;
				break;
			
			case OpCodeTag_END:
				return S->head;
			
			case OpCodeTag_RET:
				POPD
				break;
				
			case OpCodeTag_APP:
				a = POP(S);
				f = POP(S);
				//~ if (C[1].tag != OpCodeTag_RET) // TCO
				PUSHD
				C = f.Clo->cod;
				E = f.Clo->env;
				V = a;
				break;
			
			case OpCodeTag_FIX:
				f = POP(S);
				x = makeValue();
				PUSH(S) = x;
				PUSH(S) = f;
				PUSH(S) = x;
				PUSHD
				C = micro;
				break;
			
			case OpCodeTag_CLO:
				PUSH(S) = makeValue_Clo(C->CLO.lbl, mkEnv(V, E));
				break;
			
			case OpCodeTag_CST:
				PUSH(S) = C->CST.val;
				break;
			
			case OpCodeTag_BOP:
				r = POP(S);
				l = POP(S);
				PUSH(S) = C->BOP.bop(l, r);
				break;
			
			default:
				printf("Instruction missing : %s\n", nameInstruction(C));
				exit(1);
		}
		
		
	}
	
}
