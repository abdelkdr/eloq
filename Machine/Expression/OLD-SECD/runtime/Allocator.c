#include "Allocator.h"

/***************************************************/
// Page
/***************************************************/
Page Page_init(Page p, Page parent, Nat64 gen, Nat64 size) {
	*p = (S_Page) {
		parent: parent,
		prev: NULL,
		next: NULL,
		gen: gen,
		lgen: gen,
		free: size,
		frees: 0,
		objects: 0,
		konts: 0,
		hits: 0,
		current: p + 1
	};
	return p;
}

Nat64 Page_count(Page here) {
	return here ? 1 + Page_count(here->next) : 0;
}

Nat64 Page_size(Page here) {
	return here ? (here->current - (void*) (here + 1)) + here->free + Page_size(here->next) : 0;
}

Nat64 Page_busy(Page here) {
	return here ? (here->current - (void*) (here + 1)) + Page_size(here->next) : 0;
}

Nat64 Page_free(Page here) {
	return here ? here->free + Page_free(here->next) : 0;
}

Nat64 Page_frees(Page here) {
	return here ? here->frees + Page_frees(here->next) : 0;
}

Nat64 Page_objects(Page here) {
	return here ? here->objects + Page_objects(here->next) : 0;
}

Nat64 Page_konts(Page here) {
	return here ? here->konts + Page_konts(here->next) : 0;
}

Nat64 Page_hits(Page here) {
	return here ? here->hits + Page_hits(here->next) : 0;
}

Nat64 Page_gen(Page here) {
	return here->next ? Page_gen(here->next) : here->gen;
}

Nat64 Page_lgen(Page here) {
	return here->lgen;
}


//~ Nat64 Page_gen(Page here) {
	//~ return here ? here->gen + Page_gen(here->next) : 0;
//~ }

//~ Nat64 Page_lgen(Page here) {
	//~ return here ? here->lgen + Page_lgen(here->next) : 0;
//~ }

Page Page_find(Page here, void* ptr) {
	return !here ? 0 : (
		( here->gen ? (void*) here <= ptr && ptr < here->current : ( (void*) here->parent <= ptr && ptr <= here->current)  ) ? here : Page_find(here->next, ptr)
	);
}

/***************************************************/
// Pages
/***************************************************/
Page Pages_preplast(Pages ps, Page p) {
	p->next = ps->first;
	ps->first = p;
	if (p->next)
		p->next->prev = p;
	else ps->last = p;
	return p;
}

Page Pages_applast(Pages ps, Page p) {
	p->prev = ps->last;
	ps->last = p;
	if ( p->prev )
		p->prev->next = p;
	else ps->first = p;
	return p;
}

Page Pages_remove(Pages ps, Page p) {

	if ( p->next )
		p->next->prev = p->prev;
	else ps->last = p->prev;

	if ( p->prev )
		p->prev->next = p->next;
	else ps->first = p->next;

	p->next = 0;
	p->prev = 0;
	return p;
}

S_Pages Pages_removeUpto(Pages ps, Page p) {

	S_Pages nps = {ps->first, p};
	ps->first = p->next;

	if ( p->next )
		p->next->prev = 0;
	else ps->last = 0;

	p->next = 0;
	return nps;
}

Page Pages_move(Pages src, Pages tgt, Page p) {
	return Pages_preplast(tgt, Pages_remove(src, p));
}

/******************************************/
// Allocator
/******************************************/
Nat64 Allocator_heuristic(Allocator a, Nat64 size) {
	Page root = a->heap.first;
	if (root) while(root->parent)
		root = root->parent;
	Nat64 h = root ? (a->heap.first->current - (void*) root) + a->heap.first->free : a->min_alloc;
	h = h > a->max_alloc ? a->max_alloc : h;
	return size > h ? size : 2 * h;
}

Page Allocator_request(Allocator a, Nat64 size) {
	Page p;
	if (a->pool && a->pool->free >= size) {
		size = a->pool->free;
		p = a->pool;
		a->pool = 0;
	} else {
		size = Allocator_heuristic(a, size);
		p = malloc(sizeof(S_Page) + size);
	}
	return Pages_preplast(&a->heap, Page_init(p, NULL, a->gen, size));
}

Page Allocator_requestPage(Allocator a, Nat64 size) {

	if ( sizeof(S_Page) + size > a->heap.first->free )
		return Allocator_request(a, size);

	size = a->heap.first->free - sizeof(S_Page);
	a->heap.first->free = 0;
	return Pages_preplast(&a->heap, Page_init(a->heap.first->current, a->heap.first, a->gen, size));
}

void Allocator_release(Allocator a) {
	Page p;
	Pages stash = &a->stash;
	while ( stash->first ) {
		p = Pages_remove(stash, stash->first);
		if (!p->gen) // virtual page
			p->parent->free += sizeof(S_Page);
		else if (p->parent)
			//~ p->parent->free += (p->current - (void*) p) + p->free;
			p->parent->free += (p->current - p->parent->current) + p->free;
			//~ p->parent->free = (p->current - p->parent->current) + p->free;
		else if (a->pool)
			free(p);
		else {
			p->free += (p->current - (void*) (p + 1));
			p->current = p + 1;
			a->pool = p;
		}
	}
}

/******************************************/
// Stashing
/******************************************/
Page Allocator_stash(Allocator a) {
	
	return 0;
	Page last = 0;
	Nat64 gmin = a->gen;
	Page p = a->heap.first; // from invariant

	//~ printf("probe \n");
	// probe
	while(p)
		if ( (p->objects && p->hits * 10 >= p->konts * 9) || p->lgen >= gmin ) {

			last = p;
			gmin = p->gen;
			p = p->next;

		} else if (p->parent)
			p = p->parent;

		else if (p == a->heap.first)
			p = p->next;

		else p = 0;

	// no or wasted work
	if ( !last || !last->prev )
		return 0;

	//~ printf("stash \n");
	// stash
	a->stash = (S_Pages) {a->heap.first, last};
	a->heap.first = last->next;
	if (a->heap.first)
		a->heap.first->prev = 0;
	else a->heap.last = 0;
	last->next = 0;

	//~ printf("unstash \n");
	// unstash
	last = a->stash.first;
	if ( !last->parent && last->free >= (Nat64) (last->current - (void*) (last + 1)) ) {
		Pages_move(&a->stash, &a->heap, last);
		last->free -= sizeof(S_Page);
		Page v = Pages_preplast(&a->stash, Page_init(last->current + last->free, last, 0, 0));
		v->current = last->current;
		v->objects = last->objects;
		v->konts = last->konts;
		v->hits = last->hits;
		last->gen = a->gen;
		last->lgen = a->gen;
		last->objects = 0;
		last->konts = 0;
		last->hits = 0;
	} else if ( !a->heap.first )
		Allocator_request(a, 0);

	return a->heap.first;

}




//~ struct S_Page {
	//~ Page		parent;
	//~ Page		prev;
	//~ Page		next;
	//~ Nat64		gen;
	//~ Nat64		lgen;
	//~ Nat64		free;
	//~ Nat64		hits;
	//~ Nat64		objects;
	//~ Nat64		konts;
	//~ void*		current;
//~ };


//~ struct S_Allocator {
	//~ Nat64		min_alloc;
	//~ Nat64		max_alloc;
	//~ Nat64		gen;
	//~ S_Pages		heap;
	//~ S_Pages		stash;
	//~ Page		pool;
	//~ Nat64		gc;
//~ };

void print_PageHeader(char* title, Page p, Nat64 i) {
	if (title) {
		printf(
			"# #        "
			"parent            "
			"self              "
			"prev              "
			"next              "
			"current           "
			"size          "
			"free          "
			"busy          "
			"objs       "
			"frees      "
			"kons       "
			"hits       "
			"gen     "
			"lgen    "
			"\n"
		);
		printf("# -------------------------------------------------");
		printf("--------------------------------------------------");
		printf("-------------------------------------------------");
		printf("-------------------------------------------------\n");
		printf("# %-8s ", title);
		printf("%-18p", NULL);
		printf("%-18p", NULL);
		printf("%-18p", NULL);
		printf("%-18p", NULL);		
		printf("%-18p", NULL);		
		printf("%-14lu", Page_size(p));
		printf("%-14lu", Page_free(p));
		printf("%-14lu", Page_busy(p));
		printf("%-11lu", Page_objects(p));	
		printf("%-11lu", Page_frees(p));		
		printf("%-11lu", Page_konts(p));
		printf("%-11lu", Page_hits(p));		
		printf("%-8lu", Page_gen(p));
		printf("%-8lu", Page_lgen(p));
		printf("\n");
		printf("# -------------------------------------------------");
		printf("--------------------------------------------------");
		printf("-------------------------------------------------");
		printf("-------------------------------------------------\n");
	} else {
		printf("# %-9lu", i);
		printf("%-18p", p->parent);
		printf("%-18p", p);
		printf("%-18p", p->prev);
		printf("%-18p", p->next);		
		printf("%-18p", p->current);		
		printf("%-14lu", p->free + (p->current - (void*) (p + 1)));
		printf("%-14lu", p->free);		
		printf("%-14lu", p->current - (void*) (p + 1));		
		printf("%-11lu", p->objects);	
		printf("%-11lu", p->frees);		
		printf("%-11lu", p->konts);
		printf("%-11lu", p->hits);		
		printf("%-8lu", p->gen);
		printf("%-8lu", p->lgen);
		printf("\n");
	}
}

void print_Pages(char* title, Pages p) {
	printf("####################################################################################################");
	printf("###################################################################################################\n");
	print_PageHeader(title, p->first, 0);
	Page current = p->first;
	Nat64 i = 0;
	while(current) {
		print_PageHeader(NULL, current, i);
		current = current->next;
		i++;
	}
}

void print_Allocator(Allocator a) {
	printf("####################################################################################################");
	printf("###################################################################################################\n");
	printf("# Allocator");
	printf("(min_alloc: %lu,", a->min_alloc);
	printf(" max_alloc: %lu,", a->max_alloc);
	printf(" saved: %lu,", a->sav);
	printf(" generation: %lu,", a->gen);
	printf(" collections: %lu,", a->gc);
	printf(" sizeof(S_Page): %lu:Ox%lx)\n", sizeof(S_Page), sizeof(S_Page));
	if (a->stash.first) print_Pages("stash", &(a->stash));
	print_Pages("heap", &(a->heap));
	printf("####################################################################################################");
	printf("###################################################################################################\n");
}







