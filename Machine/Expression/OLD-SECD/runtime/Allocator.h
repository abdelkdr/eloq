/**********************************************/
//
/**********************************************/
typedef struct S_Page*			Page;
typedef struct S_Page			S_Page;
typedef struct S_Pages*			Pages;
typedef struct S_Pages			S_Pages;
typedef struct S_Allocator*		Allocator;
typedef struct S_Allocator		S_Allocator;

/**********************************************/
//
/**********************************************/
struct S_Pages {
	Page		first;
	Page		last;
};

struct S_Allocator {
	Nat64		min_alloc;
	Nat64		max_alloc;
	Nat64		sav;
	Nat64		gen;
	Nat64		gc;
	S_Pages		heap;
	S_Pages		stash;
	Page		pool;
};

struct S_Page {
	Page		parent;
	Page		prev;
	Page		next;
	Nat64		gen;
	Nat64		lgen;
	Nat64		free;
	Nat64		frees;
	Nat64		hits;
	Nat64		objects;
	Nat64		konts;
	void*		current;
};

/***************************************************/
// Page
/***************************************************/
Page Page_init(Page p, Page parent, Nat64 gen, Nat64 size);
Nat64 Page_count(Page here);
Nat64 Page_size(Page here);
Nat64 Page_busy(Page here);
Nat64 Page_free(Page here);
Nat64 Page_objects(Page here);
Nat64 Page_konts(Page here);
Nat64 Page_hits(Page here);
//~ Nat64 Page_gen(Page here);
//~ Nat64 Page_lgen(Page here);
Page Page_find(Page here, void* ptr);

/***************************************************/
// Pages
/***************************************************/
Page Pages_preplast(Pages ps, Page p);
Page Pages_applast(Pages ps, Page p);
Page Pages_remove(Pages ps, Page p);
S_Pages Pages_removeUpto(Pages ps, Page p);
Page Pages_move(Pages src, Pages tgt, Page p);

/******************************************/
// Allocator
/******************************************/
Nat64 Allocator_heuristic(Allocator a, Nat64 size);
Page Allocator_request(Allocator a, Nat64 size);
Page Allocator_requestPage(Allocator a, Nat64 size);
void Allocator_release(Allocator a);
Page Allocator_stash(Allocator a);
void print_Allocator(Allocator);