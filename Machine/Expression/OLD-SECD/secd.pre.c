# 0 "auto/runner.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "auto/runner.c"

Value runner(Values E) {

 printf("New Runner instance\n");

 StackFrame S = allocStack(NULL);
 KontFrame K = allocKont(NULL);
 Value A = (Value) (S_Value*) 42;
 Value Z = (Value) (S_Value*) 43;
 void* C;
 int step = 0;

 int getStack() {
  return S->current - ((Value*) (S + 1));
 }

 int getKonts() {
  return K->current - ((S_Value_Kon*) (K + 1));
 }

 void printState(char* name) {
  step++;
  printf("STEP %4i: %8s \t\t(%3i, %2i) (A= %3s, Z= %3s)\n", step, name, getStack(), getKonts(), nameValue(A), nameValue(Z));
 }

 initValue_Kon(K->current, &&RETURN, E, A); K->current++;

 Value rx0, rx1, rx2, rx3, rx4, rx5, rx6, rx7, rx8, rx9, rx10, rx11, rx12, rx13, rx14, rx15, rx16, rx17, rx18, rx19, rx20;

 goto MAIN;

 RETURN:
  printState("RETURN");
  return Z;

 FIX:
  printState("FIX");
  rx0 = makeValue();
  initValue_Kon(K->current, &&FIZ, E, rx0); K->current++;
  E = (A).Clo->env; C = (A).Clo->cod; A = rx0; goto *C;

 FIZ:
  printState("FIZ");
  copyValue(Z, A);
  Z = Z; K->current--; E = (((Value) K->current)).Clo->env; C = (((Value) K->current)).Clo->cod; A = K->current->arg; goto *C;

 PROGRAM:

  KON_18:
   printState("KON_18");

   rx0 = *(S->current -2);
   rx1 = *(S->current -1);
   rx2 = BOP_PLUS(rx1, Z);
   rx3 = BOP_PLUS(rx0, rx2);
   S->current -= 2;
   Z = rx3; K->current--; E = (((Value) K->current)).Clo->env; C = (((Value) K->current)).Clo->cod; A = K->current->arg; goto *C;

  KON_19:
   printState("KON_19");

   rx4 = getValues(E, 0);
   rx5 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
   rx6 = BOP_MINUS(A, rx5);
   initValue_Kon(K->current, &&KON_18, E, A); K->current++;
   E = (rx4).Clo->env; C = (rx4).Clo->cod; A = rx6; goto *C;

  KON_20:
   printState("KON_20");

   rx7 = getValues(E, 0);
   initValue_Kon(K->current, &&KON_19, E, A); K->current++;
   E = (rx7).Clo->env; C = (rx7).Clo->cod; A = Z; goto *C;

  KON_21:
   printState("KON_21");

   rx8 = getValues(E, 0);
   rx9 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
   rx10 = BOP_MINUS(A, rx9);
   initValue_Kon(K->current, &&KON_20, E, A); K->current++;
   E = (rx8).Clo->env; C = (rx8).Clo->cod; A = rx10; goto *C;

  LAM_22:
   printState("LAM_22");

   rx11 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 0}});
   rx12 = BOP_LTE(A, rx11);
   if (rx12.Boo.boo) {

    S->current += 0;
    rx13 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
    Z = rx13; K->current--; E = (((Value) K->current)).Clo->env; C = (((Value) K->current)).Clo->cod; A = K->current->arg; goto *C;
   } else {


    rx14 = getValues(E, 0);
    rx15 = ((Value) {Nat: {tag: ValueTag_Nat, nat: 1}});
    rx16 = BOP_MINUS(A, rx15);
    initValue_Kon(K->current, &&KON_21, E, A); K->current++;
    E = (rx14).Clo->env; C = (rx14).Clo->cod; A = rx16; goto *C;
   }

  LAM_24:
   printState("LAM_24");
   rx17 = makeValue_Clo(&&LAM_22, mkEnv(A, E));
   Z = rx17; K->current--; E = (((Value) K->current)).Clo->env; C = (((Value) K->current)).Clo->cod; A = K->current->arg; goto *C;

  KON_27:
   printState("KON_27");
   S->current -= 0;
   Z = Z; K->current--; E = (((Value) K->current)).Clo->env; C = (((Value) K->current)).Clo->cod; A = K->current->arg; goto *C;

  KON_28:
   printState("KON_28");
   rx18 = ((Value) {Nat: {tag: ValueTag_Nat, nat: N}});
   initValue_Kon(K->current, &&KON_27, E, A); K->current++;
   E = (Z).Clo->env; C = (Z).Clo->cod; A = rx18; goto *C;

  MAIN:
   printState("MAIN");
   rx19 = makeValue_Clo(&&FIX, mkEnv(A, NULL));
   rx20 = makeValue_Clo(&&LAM_24, mkEnv(A, E));
   initValue_Kon(K->current, &&KON_28, E, A); K->current++;
   E = (rx19).Clo->env; C = (rx19).Clo->cod; A = rx20; goto *C;

}
