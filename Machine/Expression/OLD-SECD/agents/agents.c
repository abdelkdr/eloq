

type enum CellTag {
	CellTag_Empty,
	CellTag_Wall,
	CellTag_Hole,
	CellTag_Food,
	CellTag_Fuel,
} CellTag;

typedef struct Map {
	Nat64	w;
	Nat64	h;
	
} S_Map;

#define PLAYER_SEGMENT(P)	(P >> 8)
#define PLAYER_POSITION(P)	(P & )

#define CELL_REMPLAYER(C, P)	C->players[PLAYER_SEGMENT(P)] &= ~PLAYER_POSITION(P)
#define CELL_ADDPLAYER(C, P)	C->players[PLAYER_SEGMENT(P)] |= PLAYER_POSITION(P)