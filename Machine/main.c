
#define min(l, r)	({__typeof__(l) _l = (l); __typeof__(r) _r = (r); _l < _r ? _l : _r;})
#define max(l, r)	({__typeof__(l) _l = (l); __typeof__(r) _r = (r); _l > _r ? _l : _r;})

#define DEBUG(X)	/*X*/

#include "Expression/Values.h"
#include "Expression/Runner.c"

//~ #include "Grammar/Parser.c"



#include "0testData/XProgram.c"
//~ #include "0testData/PEGProgram.c"
//~ #include "0testData/input_stream.c"

int strlen_(char* s) {
	int n = 0;
	while(*s++) n++;
	return n;
}

//~ void test_parser() {
	//~ char* input_stream = "1+123456789+2+3+123456789000000000+3";
	//~ Value res = parse(PEGProgram, &STRCT(Str, strlen_(input_stream), input_stream));
	//~ printf("RESULT : %li\n", res->Int.val);
//~ }

void test_runner() {
	Value_Arr env = &(S_Value_Arr) {tag: ValueTag_Arr, len: 0, arr: NULL};
	Value res = SECD_start(XProgram, NULL);
	printf("RESULT : tag: %s,  val : %li\n", Value_Names(res), res.Int.val);
}

int main(void) {
	setbuf(stdout, NULL);
	printf("sizeof instruction: %lu, sizeof U_Value: %lu, sizeof S_Value: %lu \n", sizeof(S_XInstruction), sizeof(U_Value), sizeof(S_Value_Cns2));
	//~ test_parser();
	test_runner();
	return 0;
}


