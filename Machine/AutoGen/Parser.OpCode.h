//**************************************/
// OpCodes
//**************************************/
typedef enum {
	PEGOpCode_NOPE,
	PEGOpCode_ERRR,
	PEGOpCode_FAIL,
	PEGOpCode_DONE,
	PEGOpCode_LABL,
	PEGOpCode_JUMP,
	PEGOpCode_JPOK,
	PEGOpCode_JPKO,
	PEGOpCode_CALL,
	PEGOpCode_RTRN,
	PEGOpCode_SPSH,
	PEGOpCode_SRST,
	PEGOpCode_SPOP,
	PEGOpCode_PTST,
	PEGOpCode_PPSH,
	PEGOpCode_PPOP,
	PEGOpCode_MGET,
	PEGOpCode_MSET,
	PEGOpCode_GROW,
	PEGOpCode_LSTN,
	PEGOpCode_LSTC,
	PEGOpCode_TPSH,
	PEGOpCode_TNXT,
	PEGOpCode_TPOP,
	PEGOpCode_ACTN,
	PEGOpCode_NM10,
	PEGOpCode_NB10,
	PEGOpCode_STRG,
	PEGOpCode_ACTP,
	PEGOpCode_ACTS
} PEGOpCode;

char* PEGOpCode_Names[] = {
	"NOPE",
	"ERRR",
	"FAIL",
	"DONE",
	"LABL",
	"JUMP",
	"JPOK",
	"JPKO",
	"CALL",
	"RTRN",
	"SPSH",
	"SRST",
	"SPOP",
	"PTST",
	"PPSH",
	"PPOP",
	"MGET",
	"MSET",
	"GROW",
	"LSTN",
	"LSTC",
	"TPSH",
	"TNXT",
	"TPOP",
	"ACTN",
	"NM10",
	"NB10",
	"STRG",
	"ACTP",
	"ACTS"
};

//****************************************************************************/
// Instructions
//****************************************************************************/
typedef struct GrowData {
	nat32		ruid;
	int32		addr;
} GrowData;

typedef union U_PEGInstructionData PEGInstructionData;
union U_PEGInstructionData {
	nat32		ruid;
	nat32		size;
	nat32		prcd;
	int32		addr;
	GrowData	grow;
	Value_Str	strg;
};

typedef struct S_PEGInstruction* PEGInstruction;
typedef struct S_PEGInstruction S_PEGInstruction;
struct S_PEGInstruction {
	PEGOpCode		cod;
	PEGInstructionData	dat;
};

