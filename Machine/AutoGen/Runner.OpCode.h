//**************************************/
// OpCodes
//**************************************/
typedef enum {
	XOpCode_NOPE,
	XOpCode_DONE,
	XOpCode_LABL,
	XOpCode_JUMP,
	XOpCode_JMPF,
	XOpCode_JMPT,
	XOpCode_LOAD,
	XOpCode_VARa,
	XOpCode_VARe,
	XOpCode_VARl,
	XOpCode_CPTa,
	XOpCode_CPTe,
	XOpCode_CPTl,
	XOpCode_MTCH,
	XOpCode_BRCH,
	XOpCode_GARD,
	XOpCode_FAIL,
	XOpCode_CLJR,
	XOpCode_KNTN,
	XOpCode_APLY,
	XOpCode_CNTN,
	XOpCode_RTRN,
	XOpCode_UOPE,
	XOpCode_BOPE,
	XOpCode_ARAY,
	XOpCode_COPY,
	XOpCode_FIXP,
	XOpCode_VRBL,
	XOpCode_CPTR
} XOpCode;

char* XOpCode_Names[] = {
	"NOPE",
	"DONE",
	"LABL",
	"JUMP",
	"JMPF",
	"JMPT",
	"LOAD",
	"VARa",
	"VARe",
	"VARl",
	"CPTa",
	"CPTe",
	"CPTl",
	"MTCH",
	"BRCH",
	"GARD",
	"FAIL",
	"CLJR",
	"KNTN",
	"APLY",
	"CNTN",
	"RTRN",
	"UOPE",
	"BOPE",
	"ARAY",
	"COPY",
	"FIXP",
	"VRBL",
	"CPTR"
};

//****************************************************************************/
// Instructions
//****************************************************************************/
typedef struct {int32 addr; nat32 size;} Cljr;
typedef union U_XInstructionData XInstructionData;
union U_XInstructionData {
	nat32		vrbl;
	int32		addr;
	int64		size;
	int64		nmbr;
	Cljr		cljr;
	Value*		valu;
	Value*		patt;
	Value		(*uope)(Value);
	Value		(*bope)(Value, Value);
};

typedef struct S_XInstruction* XInstruction;
typedef struct S_XInstruction S_XInstruction;
struct S_XInstruction {
	XOpCode			cod;
	XInstructionData	dat;
};

