#include "Parser.Memo.h"

MemPage MAllocator_alloc(nat64 size, MemPage next) {
	MemPage a = malloc(sizeof(S_MemPage) + size * sizeof(S_MemItem));
	*a = (S_MemPage) {size: size, free: size, next: next, curr: (MemItem) (a + 1)};
	return a;
}

void MAllocator_free(MemPage m) {
	do {
		MemPage p = m->next;
		free(m);
		m = p;
	} while(m);
}

MemItem Memo_push(MemPage* m) {
	if (!(*m)->free)
		*m = MAllocator_alloc(2 * (*m)->size, *m);
	(*m)->free--;
	return (*m)->curr++;
}

MemItem Memo_get(MemPage m, nat32 ruid, nat32 prcd, char* strt) {
	for(; m; m = m->next)
		for(MemItem i = (MemItem) (m + 1); i < m->curr; i++)
			if (i->ruid == ruid && i->prcd == prcd && i->strt == strt)
				return i;
	return NULL;
}

MemItem Memo_set(MemPage* m, nat32 ruid, nat32 prcd, char* strt, char* stop, Value rslt) {
	MemItem i = Memo_get(*m, ruid, prcd, strt);
	if ( i ) {
		i->stop = stop;
		i->rslt = rslt;
	} else {
		i = Memo_push(m);
		*i = (S_MemItem) {ruid: ruid, prcd: prcd, strt: strt, stop: stop, rslt: rslt};
	}
	return i;
}
