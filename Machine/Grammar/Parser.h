//****************************************************************************/
// Instructions
//****************************************************************************/
#include "../AutoGen/Parser.OpCode.h"

//****************************************************************************/
// Stack
//****************************************************************************/

//**************************************/
// Frame
//**************************************/
typedef struct Frame Frame;
struct Frame {
	char*		strt;
	PEGInstruction	ctrl;
};

//**************************************/
// State
//**************************************/
typedef struct State State;
struct State {
	char*		curr;
	nat32		prcd;
};

//**************************************/
// StackData
//**************************************/
typedef union StackData StackData;
union StackData {
	void**		data;
	nat32*		prcd;
	Value**		value;
	State*		state;
	Frame*		frame;
};

//**************************************/
// Stack
//**************************************/
typedef struct Stack* Stack;
typedef struct Stack S_Stack;
struct Stack {
	nat64		dim;
	nat32		size;
	nat32		free;
	Stack		prev;
	Stack		next;
	StackData	data;
};
