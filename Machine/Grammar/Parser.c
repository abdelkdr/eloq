
#include "Parser.h"
#include "Parser.Memo.c"

//**************************************/
//
//**************************************/
Stack PAllocator_alloc(nat32 size, nat64 dim, Stack next) {
	Stack p = malloc(sizeof(S_Stack) + size * dim);
	*p = (S_Stack) {dim: dim, size: size, free: size, prev: NULL, next: next, data: {data: (void**) (p + 1)}};
	return p;
}

void PAllocator_push(Stack* stack) {
	if ((*stack)->prev) {
		*stack = (*stack)->prev;
		return;
	}
	nat32 size = 2 * (*stack)->size;
	Stack p = PAllocator_alloc(size, (*stack)->dim, *stack);
	(*stack)->prev = p;
	*stack = p;
}

void PAllocator_free(Stack* stack, int all) {
	if ((*stack)->prev) {
		free((*stack)->prev);
		(*stack)->prev = NULL;
	}
	if (!all)
		*stack = (*stack)->next;
	else free(*stack);
}

//**************************************/
//
//**************************************/
Value parse(PEGInstruction ctrl, Value_Str strg) {

	Stack		prcdStack = PAllocator_alloc(128, sizeof(nat32), NULL);
	Stack		tuplStack = PAllocator_alloc(128, sizeof(Value), NULL);
	Stack		statStack = PAllocator_alloc(128, sizeof(State), NULL);
	Stack		callStack = PAllocator_alloc(128, sizeof(Frame), NULL);
	MemPage		memoTable = MAllocator_alloc(128, NULL);

	Value		result = NULL;
	MemItem		memo = NULL;

	nat32		prcd = 0;
	char*		strt = strg->str;
	char*		stop = strg->str + strg->len;
	char*		curr = strt;
	Value*		rslt = &result;

	for (nat64 step = 0; DEBUG(printf("PROCESS STEP %05lu : OpCode(%s) @ %p  => \n", step, PEGOpCode_Names[ctrl->cod], ctrl)); step++, ctrl++)
		switch(ctrl->cod) {

			//**************************************/
			//
			//**************************************/
			case PEGOpCode_DONE:
				printf("STEPS : %i\n", step);
				MAllocator_free(memoTable);
				PAllocator_free(&prcdStack, 1);
				PAllocator_free(&tuplStack, 1);
				PAllocator_free(&statStack, 1);
				PAllocator_free(&callStack, 1);
				return result;

			//**************************************/
			//
			//**************************************/
			case PEGOpCode_NOPE:
				break;

			case PEGOpCode_FAIL:
				*rslt = NULL;
				break;

			//**************************************/
			// JUMP
			//**************************************/
			case PEGOpCode_LABL:
				break;

			case PEGOpCode_JUMP:
				ctrl += ctrl->dat.addr;
				break;

			case PEGOpCode_JPOK:
				if ( *rslt ) ctrl += ctrl->dat.addr;
				break;

			case PEGOpCode_JPKO:
				if ( !*rslt ) ctrl += ctrl->dat.addr;
				break;

			//**************************************/
			// CALL
			//**************************************/
			case PEGOpCode_CALL:
				if (!callStack->free)
					PAllocator_push(&callStack);
				*(callStack->data.frame) = (Frame) {strt: strt, ctrl: ctrl};
				callStack->data.frame++;
				callStack->free--;
				strt = curr;
				ctrl += ctrl->dat.addr;
				break;

			case PEGOpCode_RTRN:
				PEGLabel_RTRN:
				if (callStack->data.data == (void**) (callStack + 1))
					PAllocator_free(&callStack, 0);
				callStack->data.frame--;
				callStack->free++;
				strt = callStack->data.frame->strt;
				ctrl = callStack->data.frame->ctrl;
				break;

			//**************************************/
			// STATE
			//**************************************/
			case PEGOpCode_SPSH:
				if (!statStack->free)
					PAllocator_push(&statStack);
				*(statStack->data.state) = (State) {curr: curr, prcd: prcd};
				statStack->data.state++;
				statStack->free--;
				break;

			case PEGOpCode_SRST:
				curr = (statStack->data.state - 1)->curr;
				prcd = (statStack->data.state - 1)->prcd;
				break;

			case PEGOpCode_SPOP:
				if (statStack->data.data == (void**) (statStack + 1))
					PAllocator_free(&statStack, 0);
				statStack->data.state--;
				statStack->free++;
				break;

			//**************************************/
			// Precedence
			//**************************************/
			case PEGOpCode_PTST:
				if (ctrl->dat.prcd > prcd || (ctrl->dat.prcd == prcd && prcd & 1))
					ctrl++;
				break;

			case PEGOpCode_PPSH:
				if (!prcdStack->free)
					PAllocator_push(&prcdStack);
				*(prcdStack->data.prcd) = prcd;
				prcdStack->data.prcd++;
				prcdStack->free--;
				prcd = ctrl->dat.prcd;
				break;

			case PEGOpCode_PPOP:
				if (prcdStack->data.data == (void**) (prcdStack + 1))
					PAllocator_free(&prcdStack, 0);
				prcdStack->data.prcd--;
				prcdStack->free++;
				prcd = *(prcdStack->data.prcd);
				break;

			//**************************************/
			// MEMO
			//**************************************/
			case PEGOpCode_MGET:
				memo = Memo_get(memoTable, ctrl->dat.ruid, prcd, strt);
				if (memo) {
					*rslt = memo->rslt;
					curr = memo->stop;
					goto PEGLabel_RTRN;
				}
				break;

			case PEGOpCode_MSET:
				Memo_set(&memoTable, ctrl->dat.ruid, prcd, strt, curr, *rslt);
				break;

			//**************************************/
			// GROW
			//**************************************/
			case PEGOpCode_GROW:
				memo = Memo_get(memoTable, ctrl->dat.ruid, prcd, strt);
				if (*rslt && curr > memo->stop) {
					Memo_set(&memoTable, ctrl->dat.ruid, prcd, strt, curr, *rslt);
					curr = memo->strt;
					ctrl+= ctrl->dat.grow.addr;
				} else {
					*rslt = memo->rslt;
					curr = memo->stop;
					//~ ctrl++;
				}
				break;

			//*****************************************/
			//* LIST
			//*****************************************/
			case PEGOpCode_LSTN:
				*rslt = Nil;
				break;

			case PEGOpCode_LSTC:
				*rslt = MAKE(Cns, *rslt, NULL);
				rslt = &((*rslt)->Cns.cdr);
				break;

			//*****************************************/
			// TUPLE
			//*****************************************/
			case PEGOpCode_TPSH:
				if (!tuplStack->free)
					PAllocator_push(&tuplStack);
				*(tuplStack->data.value) = rslt;
				tuplStack->data.value++;
				tuplStack->free--;
				*rslt = NEW(Tup, ctrl->dat.size);
				rslt = (*rslt)->Tup.tup;
				*rslt = NULL;
				break;

			case PEGOpCode_TNXT:
				rslt++;
				*rslt = NULL;
				break;

			case PEGOpCode_TPOP:
				if (tuplStack->data.data == (void**) (tuplStack + 1))
					PAllocator_free(&tuplStack, 0);
				tuplStack->data.value--;
				tuplStack->free++;
				rslt = *(tuplStack->data.value);
				break;

			//*****************************************/
			// PARSERS
			//*****************************************/
			case PEGOpCode_STRG:
				for(int x = 0, l = ctrl->dat.strg->len; x < l; x++)
					if ((curr + x) >= stop || curr[x] != ctrl->dat.strg->str[x])
						goto PEGLabel_STRG_END;
				*rslt = (Value) ctrl->dat.strg;
				curr += ctrl->dat.strg->len;
				PEGLabel_STRG_END:
				break;

			//*****************************************/
			//
			//*****************************************/
			case PEGOpCode_ACTN:
				*rslt = MAKE(Int, rslt[0]->Tup.tup[0]->Int.val * 10 + rslt[0]->Tup.tup[1]->Int.val);
				break;

			case PEGOpCode_ACTP:
				*rslt = MAKE(Int, rslt[0]->Tup.tup[0]->Int.val * rslt[0]->Tup.tup[2]->Int.val);
				break;

			case PEGOpCode_ACTS:
				*rslt = MAKE(Int, rslt[0]->Tup.tup[0]->Int.val + rslt[0]->Tup.tup[2]->Int.val);
				break;

			//*****************************************/
			// OPTI PARSERS BASE 10
			//*****************************************/
			case PEGOpCode_NM10:
				if (curr < stop && '0' <= *curr && *curr <= '9') {
					*rslt = MAKE(Int, *curr - '0');
					curr++;
				} else *rslt = NULL;
				break;

			case PEGOpCode_NB10: {
				int64 n = 0;
				if (curr < stop && '0' <= *curr && *curr <= '9') {
					do {
						n *= 10;
						n += *curr - '0';
						curr++;
					} while(curr < stop && '0' <= *curr && *curr <= '9');
					*rslt = MAKE(Int, n);
				} else *rslt = NULL;
				break;
			}

			//*****************************************/
			// OPTI PARSERS BASE 16
			//*****************************************/
			case PEGOpCode_NM16:
				if (curr < stop) {
					if ('0' <= *curr && *curr <= '9') {
						*rslt = MAKE(Int, *curr - '0');
						curr++;
					} else if ('a' <= *curr && *curr <= 'f') {
						*rslt = MAKE(Int, *curr - 'a' + 10);
						curr++;
					} else if ('A' <= *curr && *curr <= 'F') {
						*rslt = MAKE(Int, *curr - 'A' + 10);
						curr++;
					} else *rslt = NULL;
				} else *rslt = NULL;
				break;

			case PEGOpCode_NB16: {
				if (curr < stop) {

					int64 n = 0;
					bool ok = true;

					if ('0' <= *curr && *curr <= '9') {
						n += *curr - '0';
						curr++;
					} else if ('a' <= *curr && *curr <= 'f') {
						n += *curr - 'a' + 10;
						curr++;
					} else if ('A' <= *curr && *curr <= 'F') {
						n += *curr - 'A' + 10;
						curr++;
					} else {
						ok = false;
						*rslt = NULL;
					}

					if (ok) {
						while(curr < stop)
							if ('0' <= *curr && *curr <= '9') {
								n *= 16;
								n += *curr - '0';
								curr++;
							} else if ('a' <= *curr && *curr <= 'f') {
								n *= 16;
								n += *curr - 'a' + 10;
								curr++;
							} else if ('A' <= *curr && *curr <= 'F') {
								n *= 16;
								n += *curr - 'A' + 10;
								curr++;
							} else break;
						*rslt = MAKE(Int, n);
					}

				} else *rslt = NULL;
				break;
			}

			//*****************************************/
			//*****************************************/
			default:
				printf("MISSING implementation for PEGOpCode(%s) !\n", PEGOpCode_Names[ctrl->cod]);
				exit(1);

		}

	return MAKE(Int, -1);

}
