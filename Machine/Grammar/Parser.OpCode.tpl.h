//**************************************/
// OpCodes
//**************************************/
typedef enum {
	{L.implode ",\n\t" $ opCodes @ L.fmap \opcode -> "PEGOpCode_{opcode}"}
} PEGOpCode;

char* PEGOpCode_Names[] = {
	{L.implode ",\n\t" $ opCodes @ L.fmap \opcode -> "\"{opcode}\""}
};

//****************************************************************************/
// Instructions
//****************************************************************************/
typedef struct GrowData {
	nat32		ruid;
	int32		addr;
} GrowData;

typedef union U_PEGInstructionData PEGInstructionData;
union U_PEGInstructionData {
	nat32		ruid;
	nat32		size;
	nat32		prcd;
	int32		addr;
	GrowData	grow;
	Value_Str	strg;
};

typedef struct S_PEGInstruction* PEGInstruction;
typedef struct S_PEGInstruction S_PEGInstruction;
struct S_PEGInstruction {
	PEGOpCode		cod;
	PEGInstructionData	dat;
};
