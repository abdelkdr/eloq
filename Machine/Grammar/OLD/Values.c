

//~ #include "Values.Macro.h"
#define SWITCH(v)			switch(v->tag)
#define CASE(Tag)			case ValueTag_##Tag

//~ #define SIZE_Int			sizeof(S_Value_Int)
//~ #define SIZE_Nil			sizeof(S_Value_Nil)
//~ #define SIZE_Cns			sizeof(S_Value_Cns)
//~ #define SIZE_Tup(len)			(sizeof(S_Value_Tup) + sizeof(Value[len]))

//~ #define S_VALUE(Type, ...)		((S_Value_##Type) {ValueTag_##Type, __VA_ARGS__})
//~ #define VALUE(Type, ...)		((Value) &S_VALUE(Type, __VA_ARGS__))

//~ #define VALUE(Tag, ...)			&((S_Value) ({Tag: STRCT(Tag, __VA_ARGS__)}))
#define VALUE(Tag, ...)			((Value) &({Tag: STRCT(Tag, __VA_ARGS__)}))
#define STRCT(Tag, ...)			((S_Value_##Tag) {tag: ValueTag_##Tag, __VA_ARGS__})


#define MAKE(Tag, ...)			({\
						struct Value_##Tag* value = malloc(sizeof(struct Value_##Tag)); \
						*value = (struct Value_##Tag){ValueTag_##Tag, __VA_ARGS__}; \
						(Value) value; \
					})


#define NEW(Tag, ...)			INIT(ALLOC(Tag, __VA_ARGS__), Tag, __VA_ARGS__)
#define INIT(this, Tag, ...)		({S_Value_##Tag* _this = this; _this->tag = ValueTag_##Tag; INIT_##Tag(_this __VA_OPT__(, __VA_ARGS__)); (Value)_this;})
#define ALLOC(Tag, ...)			malloc(SIZE(Tag, __VA_ARGS__))
#define SIZE(Tag, ...)			(sizeof(S_Value_##Tag) SIZE_##Tag __VA_OPT__((__VA_ARGS__)))

#define INIT_Int(this)
#define INIT_Nil(this)
#define INIT_Cns(this)
#define INIT_Tup(this, param_len)	this->len = param_len; this->tup = (Value*)(this + 1);


#define SIZE_Int
#define SIZE_Nil
#define SIZE_Cns
#define SIZE_Tup(param_len)		+ sizeof(Value[param_len])


/********************************************************/
/* VALUES
/********************************************************/
typedef union Value* Value;
typedef union Value S_Value;
typedef Value* Values;

typedef struct Value_Nil* Value_Nil;
typedef struct Value_Nil S_Value_Nil;
typedef struct Value_Cns* Value_Cns;
typedef struct Value_Cns S_Value_Cns;
typedef struct Value_Int* Value_Int;
typedef struct Value_Int S_Value_Int;
typedef struct Value_Tup* Value_Tup;
typedef struct Value_Tup S_Value_Tup;

typedef struct Value_Pai* Value_Pai;
typedef struct Value_Pai S_Value_Pai;

typedef enum {
	ValueTag_Int,
	ValueTag_Nil,
	ValueTag_Cns,
	ValueTag_Tup,
	ValueTag_Pai,
} ValueTag;

struct Value_Nil {
	ValueTag	tag;
};

struct Value_Cns {
	ValueTag	tag;
	Value		car;
	Value		cdr;
};

struct Value_Int {
	ValueTag	tag;
	int64		val;
};

struct Value_Tup {
	ValueTag	tag;
	nat32		len;
	Values		tup;
};

struct Value_Pai {
	ValueTag	tag;
	Value		lft;
	Value		rgt;
};

union Value {
	ValueTag	tag;
	S_Value_Nil	Nil;
	S_Value_Cns	Cns;
	S_Value_Int	Int;
	S_Value_Tup	Tup;
	S_Value_Pai	Pai;
};



/*******************************************************/
// GETSIZE
/*******************************************************/
char* Value_getName(Value this) {
	SWITCH(this) {
		CASE(Int): return "Int";
		CASE(Nil): return "Nil";
		CASE(Cns): return "Cns";
		default: printf("\n\nERROR : Value_getName not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// GETSIZE
/*******************************************************/
nat64 Value_getSize(Value this) {
	switch(this->tag) {
		case ValueTag_Nil: return sizeof(S_Value_Nil);
		case ValueTag_Cns: return sizeof(S_Value_Cns);
		case ValueTag_Tup: return sizeof(S_Value_Tup) + sizeof(Value[this->Tup.len]);
		//~ CASE(Kont): return sizeof(S_Value_Kont) + sizeof(Value[this->Kont.size]) + 0;
		//~ CASE(Closure): return sizeof(S_Value_Closure) + sizeof(Value[this->Closure.size]) + 0;
		//~ CASE(Boo): return sizeof(S_Value_Boo) + 0;
		//~ CASE(Nat64): return sizeof(S_Value_Nat64) + 0;
		//~ CASE(Nil): return sizeof(S_Value_Nil);
		//~ CASE(Cons): return sizeof(S_Value_Cons);
		//~ CASE(Zer): return sizeof(S_Value_Zer) + 0;
		//~ CASE(Suc): return sizeof(S_Value_Suc) + 0;
		//~ CASE(Num): return sizeof(S_Value_Num) + 0;
		//~ DEFERROR(getSize, this); return 0;
		default: printf("Value_getSize not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// NEW
/*******************************************************/
Value Value_new(Value this) {
	Value new = malloc(Value_getSize(this));
	//~ printf("size: %lu => %lu, new : %p, tup: %p \n", sizeof(new->Tup), sizeof(S_Value_Tup), new, &(new->Tup) + 1);
	switch(this->tag) {
		case ValueTag_Cns: new->Cns = this->Cns; return new;
		case ValueTag_Tup: new->Tup = this->Tup; new->Tup.tup = (Value*) (&(new->Tup) + 1); return new;
		default: printf("Value_new not implemented for some type !\n"); exit(1);
	}
}

/*******************************************************/
// COPY
/*******************************************************/
Value Value_copy(Value src, Value tgt) {
	switch(src->tag) {
		//~ case ValueTag_Nil: tgt->Nil = src->Nil;	return tgt;
		//~ case ValueTag_Cns: return sizeof(S_Value_Cns);
		//~ case ValueTag_Tup: return sizeof(S_Value_Tup) + sizeof(Value[this->Tup.len]);
		default: printf("Value_copy not implemented for some type !\n"); exit(1);
	}
}

Value* Value_Iterator_init(Value this) {
	switch(this->tag) {
		case ValueTag_Cns: return &(this->Cns.car);
		case ValueTag_Tup: return this->Tup.tup;
		default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	}
}

Value* Value_Iterator_next(Value this, Value* it) {
	switch(this->tag) {
		case ValueTag_Cns: return it + 1;
		case ValueTag_Tup: return it + 1;
		default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	}
}

//~ Value* Value_Iterator_valid(Value this, Value* it) {
	//~ switch(this->tag) {
		//~ case ValueTag_Cns: return it + 1;
		//~ case ValueTag_Tup: return it + 1;
		//~ default: printf("Value_Iterator_init not implemented for some type !\n"); exit(1);
	//~ }
//~ }


/*******************************************************/
// DUMP
/*******************************************************/
void Value_dump(Value this, nat64 i) {
	printf("# %lu %s	%p %s	", i, i < 100000 ? "\t" : "", this, this < (Value) 0x100000 ? "\t" : "");
	if (this) {
		printf(
			"%s(size:%lu){",
			Value_getName(this),
			Value_getSize(this)
		);
		switch(this->tag) {
			CASE(Nil):
				break;
			CASE(Cns):
				printf("car: %p, ", this->Cns.car);
				printf("cdr: %p", this->Cns.cdr);
				break;

			default: printf("Value_dump not implemented for some type !\n"); exit(1);
		}
		printf("}\n");
	} else printf("# EMPTY VALUE\n");
}




void ListInt_print(Value this) {
	printf("(");
	for(;;) {
		printf("%li", this->Cns.car->Int.val);
		this = this->Cns.cdr;
		if (this->tag == ValueTag_Cns)
			printf(", ");
		else break;
	}
	printf(")");
}



Value Nil = &(S_Value) {Nil: {tag: ValueTag_Nil}};







