
typedef struct HMItem	HMItem;

struct HMItem {
	nat32		rule;
	nat32		prcd;
	char*		strt;
	char*		stop;
	void*		data;
	HMItem*		next;
};

typedef struct S_Page*	Page;
struct S_Page {
	Page		next;
	nat64		free;
	HMItem*		current;
};

struct S_Page* allocator = NULL;

HMItem* Allocator_push() {
	if (allocator && allocator->free) {
		allocator->free--;
		return allocator->current++;
	}
	//~ printf("OK\n");
	nat64 size = allocator ? (char*)allocator->current - ((char*)(allocator + 1)) : sizeof(HMItem);
	struct S_Page* a = malloc(size * 2 + sizeof(struct S_Page));
	*a = (struct S_Page) {next: allocator, free: size / sizeof(HMItem), current: (HMItem*) (a + 1)};
	allocator = a;
	//~ printf("OK\n");
	return Allocator_push();
}

void Allocator_pop() {
	if (allocator->current > (HMItem*)(allocator + 1)) {
		allocator->free++;
		allocator->current--;
		return;
	}
	struct S_Page* a = allocator;
	allocator = a->next;
	free(a);
	Allocator_pop();
}

HMItem* Memo_get(HMItem* m, nat32 rule, nat32 prcd, char* strt) {
	while(m)
		if (m->rule == rule && m->prcd == prcd && m->strt == strt)
			return m;
		else m = m->next;
	return NULL;
}

HMItem* Memo_add(HMItem** m, nat32 rule, nat32 prcd, char* strt, char* stop, void* data) {
	HMItem* item = Allocator_push();
	*item = (HMItem) {rule: rule, prcd: prcd, strt: strt, stop: stop, data: data, next: *m};
	*m = item;
	return item;
}

HMItem* Memo_set(HMItem* m, char* stop, void* data) {
	m->stop = stop;
	m->data = data;
	return m;
}

