#include "stdio.h"
#include "stdlib.h"

#define DEBUG(x)	/*x*/

typedef long unsigned int nat64;
typedef unsigned int nat32;
typedef long int int64;
typedef int int32;

#include "HashMap.lol.c"
#include "Values.c"
/********************************************************/
/* OPCODE
/********************************************************/

typedef enum {
	NOPE,
	FAIL,
	NM10,
	NB10,
	STRG,
	ACTN,
	LABL,
	CALL,
	RTRN,
	DONE,
	ERRR,
	JUMP,
	JPMM,
	JPOK,
	JPKO,
	MGET,
	MADD,
	GRWz,
	GRWu,
	PSHs,
	RSTs,
	POPs,
	LSTc,
	LSTn,
	TPSH,
	TNXT,
	TPOP,
} OpCodeTag;

char* OpCode_Names[] = {
	"NOPE",
	"FAIL",
	"NM10",
	"NB10",
	"STRG",
	"ACTN",
	"LABL",
	"CALL",
	"RTRN",
	"DONE",
	"ERRR",
	"JUMP",
	"JPMM",
	"JPOK",
	"JPKO",
	"MGET",
	"MADD",
	"GRWz",
	"GRWu",
	"PSHs",
	"RSTs",
	"POPs",
	"LSTc",
	"LSTn",
	"TPSH",
	"TNXT",
	"TPOP",
};

typedef union OpCode OpCode;
typedef struct OpCode_VAL S_OpCode_VAL;
typedef struct OpCode_SIZ S_OpCode_SIZ;
typedef struct OpCode_ADR S_OpCode_ADR;
typedef struct OpCode_STR S_OpCode_STR;

struct OpCode_VAL {
	OpCodeTag	tag;
	Value		val;
};

struct OpCode_SIZ {
	OpCodeTag	tag;
	nat32		siz;
};

struct OpCode_ADR {
	OpCodeTag	tag;
	int32		adr;
};

struct OpCode_STR {
	OpCodeTag	tag;
	nat64		len;
	char*		str;
};

union OpCode {
	OpCodeTag	tag;
	S_OpCode_SIZ	SIZ;
	S_OpCode_ADR	ADR;
	S_OpCode_STR	STR;
	S_OpCode_VAL	VAL;
};


/********************************************************/
/* VM REGISTERS
/********************************************************/

// VALUE STACK
typedef struct ResultStack* ResultStack;
typedef struct ResultStack S_ResultStack;
struct ResultStack {
	Value*		rslt;
	ResultStack	rstk;
};


// STATE STACK
typedef struct StateStack* StateStack;
typedef struct StateStack S_StateStack;
struct StateStack {
	char*		curr;
	int32		skip;
	StateStack	sstk;
};

// CALL STACK
typedef struct CallStack* CallStack;
typedef struct CallStack S_CallStack;
struct CallStack {
	OpCode*		ctrl;
	CallStack	stck;
};

typedef struct GrowPage* GrowPage;
typedef struct GrowPage S_GrowPage;
struct GrowPage {
	nat32		size;
	nat32		free;
	GrowPage	next;
	HMItem		items[];
};

// GROW STACK
typedef struct GrowStack* GrowStack;
typedef struct GrowStack S_GrowStack;
struct GrowStack {
	nat32		gwid;
	char*		strt;
	char*		stop;
	HMItem*		item;
	Value		rslt;
	GrowStack	gstk;
};

Value parse(OpCode* ctrl, char* curr) {

	GrowStack	gstk = NULL; //&(S_GrowStack){gwid: 0, strt: NULL, stop: NULL, gstk: NULL};
	StateStack	sstk = NULL;
	ResultStack	_rstk = NULL;
	CallStack	_stck = NULL;
	ResultStack	rstk = NULL;
	CallStack	stck = NULL;
	Value		rtrn = NULL;
	Value*		rslt = &rtrn;
	HMItem*		memo = NULL;
	HMItem*		item = NULL;

	for(int i = 0; DEBUG(printf("PROCESS STEP %05i : OpCode(%s)\n", i, OpCode_Names[ctrl->tag])); ctrl++, i++) switch(ctrl->tag) {

		case DONE:
			printf("STEPS : %i\n", i);
			return rtrn;

		case FAIL:
			*rslt = NULL;
			break;

		/*****************************************/
		/* CONTROL
		/*****************************************/
		case LABL:
			break;

		case JUMP:
			ctrl += ctrl->ADR.adr;
			break;

		case JPMM:
			if ( item ) ctrl += ctrl->ADR.adr;
			break;

		case JPOK:
			if ( *rslt ) ctrl += ctrl->ADR.adr;
			break;

		case JPKO:
			if ( !*rslt ) ctrl += ctrl->ADR.adr;
			break;

		case CALL:
			_stck = malloc(sizeof(S_CallStack));
			*_stck = (S_CallStack) {ctrl: ctrl, stck: stck};
			ctrl += ctrl->ADR.adr;
			stck = _stck;
			break;

		case RTRN:
			ctrl = stck->ctrl;
			stck = stck->stck;
			break;


		/*****************************************/
		/* LIST
		/*****************************************/
		case LSTc:
			//~ *rslt = Value_new(VALUE(Cns, car: *rslt, cdr: NULL));
			*rslt = MAKE(Cns, *rslt, NULL);
			rslt = &((*rslt)->Cns.cdr);
			break;

		case LSTn:
			*rslt = Nil;
			break;

		/*****************************************/
		/* TUPLE
		/*****************************************/
		case TPSH:
			_rstk = malloc(sizeof(S_ResultStack));
			*_rstk = (S_ResultStack) {rslt: rslt, rstk: rstk};
			//~ *rslt = Value_new(VALUE(Tup, len: ctrl->SIZ.siz));
			*rslt = NEW(Tup, ctrl->SIZ.siz);
			rslt = (*rslt)->Tup.tup;
			rstk = _rstk;
			break;

		case TNXT:
			rslt++;
			break;

		case TPOP:
			_rstk = rstk;
			rslt = rstk->rslt;
			rstk = rstk->rstk;
			free(_rstk);
			break;

		/*****************************************/
		/* MEMOIZATION
		/*****************************************/
		case MGET:
			item = Memo_get(memo, 0, 0, curr);
			if (item) {
				*rslt = item->data;
				curr = item->stop;
			}
			break;

		case MADD:
			item = Memo_add(&memo, 0, 0, curr, curr, NULL);
			break;

		case GRWz:
		{
			GrowStack g = malloc(sizeof(S_GrowStack));
			*g = (S_GrowStack) {item: item, gstk: gstk};
			gstk = g;
		}
		break;

		case GRWu:
			//~ item = Memo_get(memo, 0, 0, gstk->strt);
			item = gstk->item;
			if (*rslt && curr > item->stop ) {
				Memo_set(item, curr, *rslt);
				curr = item->strt;
				ctrl += ctrl->ADR.adr;
			} else {
				*rslt = item->data;
				gstk = gstk->gstk;
			}
		break;

		case PSHs: {
			StateStack s = malloc(sizeof(S_StateStack));
			*s = (S_StateStack) {curr: curr, sstk: sstk};
			sstk = s;
		}
		break;

		case RSTs:
			curr = sstk->curr;
		break;

		case POPs:
			sstk = sstk->sstk;
		break;


		/*****************************************/
		/* PARSERS
		/*****************************************/
		case NM10:
			if (*curr && '0' <= *curr && *curr <= '9') {
				*rslt = MAKE(Int, *curr - '0');
				curr++;
			} else *rslt = NULL;
		break;

		case NB10: {
			int64 n = 0;
			if (*curr && '0' <= *curr && *curr <= '9') {
				do {
					n *= 10;
					n += *curr - '0';
					curr++;
				} while(*curr && '0' <= *curr && *curr <= '9');
				*rslt = MAKE(Int, n);
			} else *rslt = NULL;
		}
		break;


		/*****************************************/
		/*
		/*****************************************/
		case ACTN:
			*rslt = MAKE(Int, rslt[0]->Tup.tup[0]->Int.val * 10 + rslt[0]->Tup.tup[1]->Int.val);
		break;

		default:
			printf("MISSING !");
			exit(1);
	}

	return MAKE(Int, -1);

}



//~ struct S_VInt* tst(int lol) {
	//~ return &(struct S_VInt){tag: ValueTag_Int, val: lol};
//~ }

#include "PROGRAM.c"
#include "INPUT.c"

int main(void) {
	//~ input = "1234567890123456789";
	//~ input = "12";
	printf("Hello World !\n");
	//~ printf("%i !\n", __builtin_constant_p(SIZE(Tup, len: 45)));
	//~ printf("%i !\n", __builtin_constant_p(sizeof(S_Value_Tup)));
	//~ printf("%i !\n", __builtin_constant_p(sizeof(Value[((S_Value_Tup){tag: ValueTag_Tup, len: 45}).len])));
	//~ printf("%i !\n", __builtin_constant_p(sizeof(Value[45]) + sizeof(Value[45])));
	//~ printf("%li !\n", (sizeof(Value[((S_Value_Tup const){tag: ValueTag_Tup, len: 45}).len])));
	//~ printf("%li !\n", (sizeof(Value[1])));
	Value res = parse(PROGRAM, input);
	//~ printf("RESULT : %i\n", res->tag);
	printf("RESULT : %li\n", res->Int.val);
	//~ ListInt_print(res);
	return 0;

	NEW(Nil);
}



