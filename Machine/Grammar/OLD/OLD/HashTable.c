#include "stdio.h"
#include "stdlib.h"


typedef		unsigned int		boo;
typedef		long int		int64;
typedef		long unsigned int	nat64;
typedef		int			int32;
typedef		unsigned int		nat32;

typedef		struct HTSlot		HTSlot;
typedef		struct HTItem		HTItem;
typedef		struct HTable*		HTable;
typedef		struct HTable		S_HTable;

struct HTItem {
	nat32		prcd;
	nat64		strt;
	nat64		stop;
	void*		data;
	HTItem*		next;
};

struct HTSlot {
	nat64		size;
	HTItem*		item;
};

struct HTable {
	nat64		seed;
	nat64		cpct;
	nat64		size;
	nat64*		repr;
	HTSlot*		slts;
};


HTable HTable_mak() {
	HTable ht = malloc(sizeof(S_HTable));
	void* mem = malloc(sizeof(nat64) + sizeof(HTSlot));
	*ht = (S_HTable) {
		seed: 0,
		cpct: 1,
		size: 0,
		repr: (nat64*) mem,
		slts: (HTSlot*) (((nat64*) mem) + 1),
	};
	ht->repr[0] = 0;
	return ht;
}

//~ boo HTable_rea(HTable ht, nat64 size) {
	//~ ht->slts = malloc(sizeof(HTSlot[size]));
	//~ for(nat64 i = 0; i < size; i++)
		//~ ht->slts[i] = (HTSlot) {0, NULL};
	//~ return 1;
//~ }

nat64 HTable_hsh(nat64 key) {
	key = (key ^ (key >> 30)) * 0xbf58476d1ce4e5b9;
	key = (key ^ (key >> 27)) * 0x94d049bb133111eb;
	key = (key ^ (key >> 31));
	return key;
}

HTItem* HTable_get(HTable ht, nat32 prcd, nat64 strt) {
	nat64 idx = HTable_hsh((strt << 8) + prcd) % ht->cpct;
	if ( !(ht->repr[idx / 64] & (1 << (idx % 64))) )
		return NULL;
	HTItem* i = ht->slts[idx].item;
	while (i)
		if (i->prcd = prcd && i->strt == strt)
			return i;
		else
			i = i->next;
	return NULL;
}


//~ boo HTable_set(HTable ht, nat32 prcd, nat64 strt, nat64 stop, void* data) {
	//~ nat64 hsh = HTable_hsh((strt << 8) + prcd);
	//~ if
//~ }






