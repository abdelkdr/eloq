

#define SIZE(Tag, ...)			SIZE_##Tag __VA_OPT__((__VA_ARGS__))

#define MAKE(Tag, ...)			INIT(NEW(Tag, __VA_ARGS__), __VA_ARGS__)
#define NEW(Tag, ...)			INIT(ALLOC(Tag, __VA_ARGS__), Tag, __VA_ARGS__)



