#include "stdio.h"
#include "stdlib.h"


#define VALUE(Tag, ...) ({ struct S_V##Tag* value = malloc(sizeof(struct S_V##Tag)); *value = (struct S_V##Tag){ValueTag_##Tag, __VA_ARGS__}; (Value) value; })


typedef struct Cons* List;
struct Cons {void* car; char* pos; char* end; List cdr;};


//~ (MEMO(0), ALT($PRD, $SUM, $INT),)

typedef unsigned int uint32;
typedef long int int64;


typedef union U_Value* Value;
typedef enum E_ValueTag ValueTag;
enum E_ValueTag {
	ValueTag_MIN,
	ValueTag_Int,
	ValueTag_Str,
	ValueTag_Pair,
};

struct S_VInt {
	ValueTag	tag;
	int64		val;
};

struct S_VStr {
	ValueTag	tag;
	int64		len;
	char*		str;
};

struct S_VPair {
	ValueTag	tag;
	Value		lft;
	Value		rgt;
};

union U_Value {
	ValueTag	tag;
	struct S_VInt	Int;
	struct S_VStr	Str;
	struct S_VPair	Pair;
};



typedef union U_Parser Parser;

typedef enum E_ParserTag ParserTag;
enum E_ParserTag {
	ParserTag_MIN,
	ParserTag_END,
	ParserTag_RET,
	ParserTag_GET,
	ParserTag_SET,
	ParserTag_MEM,
	ParserTag_N10,
	ParserTag_Str,
	ParserTag_BWS,
	ParserTag_CAL,
	ParserTag_TUP,
	ParserTag_TUP0,
	ParserTag_TUP1,
	ParserTag_TUP_SUCC,
	ParserTag_CLRVALUE,
	ParserTag_JMP,
	ParserTag_JMPF,
	ParserTag_ACT,
	ParserTag_PSHPOS,
	ParserTag_POPPOS,
	ParserTag_SETPOS,
	//~ ParserTag_Seq,
	//~ ParserTag_Alt,
	//~ ParserTag_,
};

//~ struct S_PN10 {
	//~ ParserTag	tag;
//~ };

struct S_PStr {
	ParserTag	tag;
	uint32		len;
	char*		str;
};

struct S_PSET {
	ParserTag	tag;
	List*		lst;
	Value		val;
};

struct S_PGET {
	ParserTag	tag;
	List*		lst;
};

struct S_PCAL {
	ParserTag	tag;
	Parser*		adr;
};

struct S_PJMP {
	ParserTag	tag;
	int		n;
};


union U_Parser {
	ParserTag	tag;
	//~ struct S_PN10	n10;
	struct S_PSET	Set;
	struct S_PGET	Get;
	struct S_PCAL	Cal;
	struct S_PStr	Str;
	struct S_PJMP	Jmp;
};



Value parse(Parser* c, char* input_current) {


	char* pstr_pattern;
	char pstr_current;
	char char_current;
	Value result = 0;
	List results = 0;
	List stack = 0;
	List pos = 0;

	for(int i = 0; printf("PROCESS INSTRUCTION %i : ", i);c++, i++) switch(c->tag) {

		case ParserTag_END:
			printf("END\n");
			return result;


		case ParserTag_N10:
			printf("N10");
			result = 0;
			char_current = *input_current;
			if (!char_current) {
				printf(" : END OF STRING\n");
				break;
			}
			printf(" : MATCH CHR : \\d on %c\n", char_current);
			if ('0' <= char_current && char_current <= '9') {
				input_current++;
				result = VALUE(Int, char_current - '0');
			} else result = 0;
			break;

		case ParserTag_Str:
			result = 0;
			for(pstr_pattern = c->Str.str; char_current = *pstr_pattern; pstr_pattern++, input_current++) {
				printf("MATCH CHR : %c on %c\n", char_current, *input_current);
				if (char_current != *input_current)
					break;
			}
			if ( !char_current )
				//~ result = &(union U_Value) {Str: {tag: ValueTag_Str, len: c->str.len, str: c->str.str}};
				result = VALUE(Str, c->Str.len, c->Str.str);
			break;


		case ParserTag_CLRVALUE:
			printf("CLRVALUE\n");
			{
				List l = results;
				results = results->cdr;
				free(l);
			}
			break;

		case ParserTag_TUP:
			printf("TUP\n");
			List l = malloc(sizeof(struct Cons));
			l->car = VALUE(Pair, NULL, NULL);
			l->cdr = results;
			results = l;
			break;

		case ParserTag_TUP0:
			printf("TUP0\n");
			((Value) results->car)->Pair.lft = result;
			break;

		case ParserTag_TUP1:
			printf("TUP1\n");
			((Value) results->car)->Pair.rgt = result;
			break;

		case ParserTag_TUP_SUCC:
			printf("TUP_SUCC\n");
			((Value) results->car)->Pair.rgt = result;
			result = results->car;
			{
				List l = results;
				results = l->cdr;
				free(l);
			}
			break;
		case ParserTag_ACT:
			printf("ACT\n");
			result = VALUE(Int, result->Pair.lft->Int.val * 10 + result->Pair.rgt->Int.val);
			break;

		case ParserTag_JMP:
			printf("JMP\n");
			c += c->Jmp.n;
			break;

		case ParserTag_JMPF:
			printf("JMPF\n");
			if (!result)
				c += c->Jmp.n;
			break;

		case ParserTag_RET:
			printf("RET\n");
			c = stack->car;
			stack = stack->cdr;
			break;

		case ParserTag_CAL:
			{
				printf("CAL\n");
				List l = malloc(sizeof(struct Cons));
				l->car = c;
				l->cdr = stack;
				stack = l;
				c = c->Cal.adr - 1;
			}
			break;

		case ParserTag_MEM:
			printf("MEM\n");
			if ( result && input_current > (*c->Set.lst)->end ) {
				(*c->Set.lst)->car = result;
				(*c->Set.lst)->end = input_current;
				input_current = (*c->Set.lst)->pos;
			} else {
				result = (*c->Set.lst)->car;
				input_current = (*c->Set.lst)->end;
				{
					List l = *c->Set.lst;
					*c->Set.lst = (*c->Set.lst)->cdr;
					free(l);
				}
				c++;
			}
			break;

		case ParserTag_GET:
			if ( !*(c->Set.lst) ) {
				printf("GET : It's GET empty !\n");
				c++;
				break;
			} else if ( (*c->Set.lst)->pos != input_current ) {
				printf("GET : It's GET not here !\n");
				c++;
				break;
			} else printf("GET\n");
			result = (*c->Set.lst)->car;
			input_current = (*c->Set.lst)->end;
			break;

		case ParserTag_SET:
			if ( !*(c->Set.lst) ) {
				printf("SET : It's SET empty !\n");
				List l = malloc(sizeof(struct Cons));
				l->car = c->Set.val;
				l->pos = input_current;
				l->end = input_current;
				l->cdr = *c->Set.lst;
				*c->Set.lst = l;
			} else if ( (*c->Set.lst)->pos != input_current ) {
				printf("SET : It's SET not here !\n");
				List l = malloc(sizeof(struct Cons));
				l->car = c->Set.val;
				l->pos = input_current;
				l->end = input_current;
				l->cdr = *c->Set.lst;
				*c->Set.lst = l;
			} else printf("SET\n");
			break;

		case ParserTag_PSHPOS:
			{
				printf("PSHPOS\n");
				List l = malloc(sizeof(struct Cons));
				l->pos = input_current;
				l->cdr = pos;
				pos = l;
			}
			break;

		case ParserTag_POPPOS:
			{
				printf("POPPOS\n");
				List l = pos;
				pos = pos->cdr;
				free(l);
			}
			break;

		case ParserTag_SETPOS:
			{
				printf("SETPOS\n");
				input_current = pos->pos;
			}
			break;

		default:
			printf("TOKEN INSTRUCTION NOT FOUND %i!\n", c->tag);
		exit(1);

	}
	printf("REACH\n");
	return result;
}


/*
	int	-> int num	=> [ this.0 * 10 + this.1 ]
		-> num
*/

List growInt = NULL;

// int -> { int num } | num
Parser IntParser[] = {
	{Get: {tag: ParserTag_GET, lst: &growInt}},
	{tag: ParserTag_RET},
	{Set: {tag: ParserTag_SET, lst: &growInt, val: NULL}},
		// { int num } | num
		{tag: ParserTag_PSHPOS},
			// { int num }
			{Cal: {tag: ParserTag_CAL, adr: IntParser}},
			{Jmp: {tag: ParserTag_JMPF, n: 10}},
			{tag: ParserTag_TUP},
			{tag: ParserTag_TUP0},
			{tag: ParserTag_N10},
			{Jmp: {tag: ParserTag_JMPF, n: 5}},
			{tag: ParserTag_TUP1},
			{tag: ParserTag_TUP_SUCC},
			{tag: ParserTag_ACT},
			{tag: ParserTag_POPPOS},
			{Jmp: {tag: ParserTag_JMP, n: 4}},
			{tag: ParserTag_CLRVALUE},
		{tag: ParserTag_SETPOS},
			// num
			{tag: ParserTag_N10},
			{tag: ParserTag_POPPOS},
	{Set: {tag: ParserTag_MEM, lst: &growInt}},
	{Jmp: {tag: ParserTag_JMP, n: -18}},
	{tag: ParserTag_RET},
};

List growExpr = NULL;
// expr -> { expr "*" 4<expr } | { expr "+" 3<expr } | int
Parser ExprParser[] = {
	{Get: {tag: ParserTag_GET, lst: &growExpr}},
	{tag: ParserTag_RET},
	{Set: {tag: ParserTag_SET, lst: &growExpr, val: NULL}},
		// { int num } | num
		{tag: ParserTag_PSHPOS},
			// { int num }
			{Cal: {tag: ParserTag_CAL, adr: IntParser}},
			{Jmp: {tag: ParserTag_JMPF, n: 10}},
			{tag: ParserTag_TUP},
			{tag: ParserTag_TUP0},
			{tag: ParserTag_N10},
			{Jmp: {tag: ParserTag_JMPF, n: 5}},
			{tag: ParserTag_TUP1},
			{tag: ParserTag_TUP_SUCC},
			{tag: ParserTag_ACT},
			{tag: ParserTag_POPPOS},
			{Jmp: {tag: ParserTag_JMP, n: 4}},
			{tag: ParserTag_CLRVALUE},
		{tag: ParserTag_SETPOS},
			// num
			{tag: ParserTag_N10},
			{tag: ParserTag_POPPOS},
	{Set: {tag: ParserTag_MEM, lst: &growInt}},
	{Jmp: {tag: ParserTag_JMP, n: -18}},
	{tag: ParserTag_RET},
};


Parser BaseParser[] = {
	{Cal: {tag: ParserTag_CAL, adr: IntParser}},
	{tag: ParserTag_END},
};


struct S_VInt* tst(int lol) {
	return &(struct S_VInt){tag: ValueTag_Int, val: lol};
}

char* input = "932045000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000";
int main(void) {
	printf("Hello World !\n");
	Value res = parse(BaseParser, input);
	printf("RESULT : %li\n", res->Int.val);
	return 0;
}