typedef struct S_MemItem* MemItem;
typedef struct S_MemItem S_MemItem;
struct S_MemItem {
	nat32		ruid;
	nat32		prcd;
	char*		strt;
	char*		stop;
	Value		rslt;
};


typedef struct S_MemPage* MemPage;
typedef struct S_MemPage S_MemPage;
struct S_MemPage {
	nat64		size;
	nat64		free;
	MemPage		next;
	MemItem		curr;
};
