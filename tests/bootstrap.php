#!/use/local/bin/php
<?php
namespace Eloq;
include('Timer.php');
include('TimerFormat.php');

function log(...$str) {
	echo date("H:i:s : "), implode(" ", $str), "\n";
}


echo "Starting self-hosting test before updating bootstrap...\n";

chdir(__DIR__);
// bootstrap test
//~ $index = __DIR__ . "/../" . $argv[1];
$boot = __DIR__ . '/../bootstraps/bootstrap.php';
$index = __DIR__ . "/../src/index";
$out = __DIR__ . "/../tmp/bootstrap.new.php";
if ( file_exists($out) ) unlink($out);
copy($boot, $out);
//####################################################################################
$auto = 0;
$timer = new Timer(new TimerFormat("{z}{u}"));
for ($i = 0; $i < 1000; $i++) {
	$obj = (include($i ? $out : $boot))();
	$ver = $obj::$eloq_version;
	$ns = $obj->nspace;
	log("BOOTED    #$i : ", $timer->getDelta());
	//####################################################################################
//~ 	$src = '<?php ' . $ns['Eloq']['dump']($index)("../bootstraps/bootstrap");
//~ 	log("dumped");
//~ 	$src = '<?php ' . $ns['Eloq']['dumpread']($index)("../bootstraps/bootstrap.read");
//~ 	log("read dumped");
	$context = ['root' => dirname($index), 'dir' => dirname($index), 'file' => '<text>', 'fileStack' => []];
	$ncode = $ns['Eloq']['compileFile']($index);
	$bcode = "<?php \$eloq_version = $ver; $ncode";
//~ 	$bcode = '<?php ' . $ns['Eloq']['compileText']($context)(file_get_contents($index));
	//####################################################
	if (file_get_contents($out) === $bcode)
		$auto++;
	else {$auto = 0; $ver++;}
	file_put_contents($out, "<?php \$eloq_version = $ver; $ncode");
	log("COMPILED  #$i => auto : #$auto : " . ($auto ? "<VERIFIED>  " : "<NORMALIZED>"), $timer->getDelta());
	echo "\n";
	//####################################################
	if ( $auto ) {
		log("SAME THRICE => UPDATING BOOTSTRAP");
		rename($boot, $boot . "." . time() . ".back");
		rename($out, $boot);
		log("SAME THRICE => UPDATE BOOTSTRAP OK");
		print_r(memory_get_usage(false));
		echo "\n";
		print_r(memory_get_usage(true));
		echo "\n";
		print_r(memory_get_peak_usage(false));
		echo "\n";
		print_r(memory_get_peak_usage(true));
		echo "\n";
		exit(0);
	}
}
exit(1);
