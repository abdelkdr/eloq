<?php

$port = 5400;
$address = "127.0.0.1";

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);

$bound = socket_bind($socket, $address, $port);
socket_listen($socket);


function startClient($client) {
	socket_write($client, "Hello\n");
}


function doRead($reading, $buffers) {
	$writing = [];
	$excepting = [];
	echo "doRead\n";
	if ($u = socket_select($writing, $reading, $excepting, 1)) {
		print_r($reading);
		echo "Reading\n";
		foreach($reading as $c => $client)
			$buffers[$c] .= socket_read($client, 1);
	}
	return $buffers;
}

function doWrite($writing, $str) {
	$reading = [];
	$excepting = [];
	echo "doWrite\n";
//~ 	if (socket_select($writing, $reading, $excepting, 1)) {
		echo "Writing\n";
		foreach($writing as $c => $client)
			socket_write($client, $str);
//~ 	}
}

function doWork($clients, $buffers, $i) {
	echo "doWork $i\n";
	$buffers = doRead($clients, $buffers);
	doWrite($clients, "update : $i\n");
	sleep(1);
	return $buffers;
}

$buffers = [];
$clients = [];
for($i = 0; $i < 40; $i++) {

	do if (socket_select($accepting = [$socket], $reading = [], $excepting = [], 0)) {

		echo "New Client !!\n";
		$client = socket_accept($socket);
		socket_set_nonblock($client);
		startClient($client);
		$clients[] = $client;
		$buffers[] = '';

	} while ($accepting);

	if ($clients) {
		$buffers = doWork($clients, $buffers, $i);
	} else {
		echo "Sleep $i\n";
		sleep(1);
	}

}

foreach ($clients as $c) {
	socket_write($c, "Bye\n");
	socket_close($c);
}

socket_close($socket);